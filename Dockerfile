FROM openjdk:8-jdk-alpine
COPY target/*.jar  hiredrive.jar
EXPOSE 8080
CMD java -jar -Dspring.profiles.active=prod ./hiredrive.jar


#FROM maven:3.5.2-jdk-8-alpine AS MAVEN_BUILD
#COPY pom.xml /build/
#COPY src /build/src/
#WORKDIR /build/
#RUN mvn package -Pprod
#FROM openjdk:8-jre-alpine
#WORKDIR /app
#COPY --from=MAVEN_BUILD /build/target/*.jar /app/hiredrive.jar
#ENTRYPOINT ["java", "-jar", "hiredrive.jar"]