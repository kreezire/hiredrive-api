package in.vipulaggarwal.hiredrive.services;

import java.util.List;

import in.vipulaggarwal.hiredrive.dto.InterviewCandidateRoundDTO;
import in.vipulaggarwal.hiredrive.dto.InterviewDTO;

public interface InterviewService {

	InterviewDTO addInterview(InterviewDTO interviewDTO);

	InterviewDTO getById(Integer id);

	InterviewDTO update(InterviewDTO interviewDTO);

	InterviewDTO getByCandidateIdAndRoundId(Integer candidateId, Integer roundId);

	Integer getCountByRoundId(Integer roundId);

	List<InterviewDTO> getInterviewByCandidateId(Integer candidateId);

	List<InterviewDTO> getInterviewAssignedToMe();

	List<InterviewCandidateRoundDTO> getInterviewCandidateRoundDetailsAssignedToMe();

	boolean isAnyRoundOngoingInDrive(Integer driveId);

	List<InterviewDTO> getByDriveIdAndInterviewer(Integer driveId, String interviewer);
}
