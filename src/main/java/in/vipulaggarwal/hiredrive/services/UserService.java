package in.vipulaggarwal.hiredrive.services;

import in.vipulaggarwal.hiredrive.dto.BasicUserAuthDTO;
import in.vipulaggarwal.hiredrive.dto.BasicUserDTO;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserService {

	BasicUserDTO add(BasicUserAuthDTO basicUserAuthDTO);

	UserDetails getUserDetailsByUsername(String username);

	BasicUserDTO getUserByUsername(String username);

	BasicUserDTO changePassword(BasicUserDTO basicUserDTO, String currentPassword, String newPassword);

	BasicUserDTO forgotPassword(String emailId);

	void resetPassword(String emailId, String token, String newPassword, String confirmPassword);

}
