package in.vipulaggarwal.hiredrive.services;

import in.vipulaggarwal.hiredrive.dto.RoundDTO;
import in.vipulaggarwal.hiredrive.enums.EntityType;

import java.util.List;

public interface RoundService {

	RoundDTO addRoundInfo(RoundDTO round, Integer entityId, EntityType entityType);

	List<RoundDTO> addRoundsInfo(List<RoundDTO> rounds, Integer entityId, EntityType entityType);

	RoundDTO getRoundByID(Integer id);

	List<RoundDTO> getRoundByEntityIdAndEntityType(Integer id, EntityType entityType);

	RoundDTO updateRoundByID(RoundDTO roundDTO);

	void delete(Integer id);
}
