package in.vipulaggarwal.hiredrive.services;

import in.vipulaggarwal.hiredrive.dto.BarChartResponseDTO;
import in.vipulaggarwal.hiredrive.dto.GroupedBarChartResponseDTO;

public interface AnalyticsService {

	BarChartResponseDTO<String> getRoundStatsForDrive(Integer driveId);
	
	BarChartResponseDTO<String> getInterviewerStatsForDrive(Integer driveId);
	
	GroupedBarChartResponseDTO<String, String> getTalentPartnerStatsForDrive(Integer driveId);
}
