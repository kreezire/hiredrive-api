package in.vipulaggarwal.hiredrive.services;

import in.vipulaggarwal.hiredrive.auth.services.AuthorityEvaluator;
import in.vipulaggarwal.hiredrive.common.ConversionService;
import in.vipulaggarwal.hiredrive.db.qo.AbstractQO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class AbstractService {

    @Autowired
    protected ConversionService conversionService;

    @Autowired
    protected AuthorityEvaluator authorityEvaluator;

    public AbstractService() {

    }


}
