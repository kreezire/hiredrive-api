package in.vipulaggarwal.hiredrive.services;

import in.vipulaggarwal.hiredrive.db.qo.CandidateQO;

public interface ResumeParser {

	Boolean isResumeProxyWorking();

	void fillCandidateQOFromResume(CandidateQO candidateQO, String fileStorageURL);
}
