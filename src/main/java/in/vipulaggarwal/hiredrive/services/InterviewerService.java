package in.vipulaggarwal.hiredrive.services;

import java.util.List;

import javax.validation.constraints.NotNull;

import in.vipulaggarwal.hiredrive.dto.InterviewerDTO;

public interface InterviewerService {

	InterviewerDTO addInterviewer(InterviewerDTO interviewerDTO);

	InterviewerDTO getById(Integer id);

	InterviewerDTO update(InterviewerDTO interviewerDTO);

	void delete(Integer id);

	List<InterviewerDTO> getInterviewersByDriveId(@NotNull Integer driveId);
}
