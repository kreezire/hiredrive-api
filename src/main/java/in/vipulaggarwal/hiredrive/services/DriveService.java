package in.vipulaggarwal.hiredrive.services;

import in.vipulaggarwal.hiredrive.dto.DriveDTO;

import java.util.List;

public interface DriveService {

	DriveDTO addDrive(DriveDTO driveDTO);

	DriveDTO getDriveByID(Integer id);

	DriveDTO updateDrive(DriveDTO driveDTO);

	List<DriveDTO> getMyDrives();

	List<DriveDTO> getAllDrives();

	List<DriveDTO> getMyTeamsDrives();
}
