package in.vipulaggarwal.hiredrive.services.impl;

import in.vipulaggarwal.hiredrive.common.utils.Utils;
import in.vipulaggarwal.hiredrive.db.qo.CandidateQO;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import in.vipulaggarwal.hiredrive.exceptions.HDHttpException;
import in.vipulaggarwal.hiredrive.services.ResumeParser;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

import javax.annotation.PostConstruct;

public class ResumeParserImpl implements ResumeParser {
	private static final Logger logger = LoggerFactory.getLogger(ResumeParserImpl.class);

	@Value("${hiredrive.resume.parser.url}")
	private String parserBaseUrl;

	private static String parserRelativePath = "resume_info_from_resume_url";

	private String parserUrl;

	@Autowired
	ProxyService proxyService;

	@PostConstruct
	private void init() {
		this.parserUrl = parserBaseUrl + "/" + parserRelativePath;
	}

	@Override
	public Boolean isResumeProxyWorking() {
		Boolean t = false;
		try {
			t = Utils.doesURLWorking(parserBaseUrl);
		} catch (IOException e) {
			logger.error("ResumeParse service is not working.", e);
		}
		return t;
	}

	public void fillCandidateQOFromResume(CandidateQO candidateQO, String fileStorageURL) {

		// create request body
		JSONObject request = new JSONObject();
		try {
			request.put("fileStorageURL", fileStorageURL);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			logger.error("Exception while creating post request json={}", e1);
			throw new HDException("Error while parsing the file.");
		}

		try {
			ResponseEntity<String> response = proxyService.call(parserUrl, HttpMethod.POST, request.toString(),
					String.class, null, MediaType.APPLICATION_JSON);
			if (response.getStatusCode() == HttpStatus.OK) {
				try {
					JSONObject candidateInfo = new JSONObject(response.getBody());
					candidateQO.setName((String) candidateInfo.get("name"));
					candidateQO.setPhone((String) candidateInfo.get("phone"));
					candidateQO.setEmailId((String) candidateInfo.get("emailId"));
				} catch (JSONException e) {
					logger.error("exception={} in parsing resume response for candidate id={}", e, candidateQO.getId());
					throw new HDException("Exception in parsing resume.");
				}
			}

		} catch (HDHttpException ex) {
			logger.error("exception={} in parsing resume for candidate id={}", ex, candidateQO.getId());
			throw new HDException("Exception in parsing resume.");
		}

	}

}
