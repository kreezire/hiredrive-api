package in.vipulaggarwal.hiredrive.services.impl;

import in.vipulaggarwal.hiredrive.common.utils.RoundsHelper;
import in.vipulaggarwal.hiredrive.db.dao.RoundDAO;
import in.vipulaggarwal.hiredrive.db.qo.RoundQO;
import in.vipulaggarwal.hiredrive.dto.RoundDTO;
import in.vipulaggarwal.hiredrive.enums.EntityType;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.services.AbstractService;
import in.vipulaggarwal.hiredrive.services.InterviewService;
import in.vipulaggarwal.hiredrive.services.RoundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;

public class RoundServiceImpl extends AbstractService implements RoundService {

	private static final Logger logger = LoggerFactory.getLogger(RoundServiceImpl.class);

	@Autowired
	private RoundDAO roundDAO;

	@Autowired
	private InterviewService interviewService;

	@Override
	public RoundDTO addRoundInfo(RoundDTO round, Integer entityId, EntityType entityType) {
		RoundDTO roundDTO = null;
		if (round != null) {
			RoundQO roundQO = conversionService.convert(round, RoundQO.class);
			RoundsHelper.addCreateUserId(roundQO, authorityEvaluator.getCurrentUsername());
			Integer id = roundDAO.addRound(roundQO);
			roundDTO = getRoundByID(id);
		}
		return roundDTO;
	}

	@Override
	public List<RoundDTO> addRoundsInfo(List<RoundDTO> rounds, Integer entityId, EntityType entityType) {
		List<RoundDTO> roundDTOs = null;
		if (rounds != null) {
			List<RoundQO> roundQOs = conversionService.convert(rounds, RoundQO.class);
			RoundsHelper.addCreateUserId(roundQOs, authorityEvaluator.getCurrentUsername());
			roundDAO.addRounds(roundQOs, entityId, entityType.toString());
			roundDTOs = getRoundByEntityIdAndEntityType(entityId, entityType);
		}
		return roundDTOs;
	}

	@Override
	public RoundDTO getRoundByID(Integer id) {
		RoundQO roundQO = roundDAO.get(id);

		if (roundQO == null) {
			throw new NotFoundException("Round with id=" + id + " was not found.");
		} else {
			return this.conversionService.convert(roundQO, RoundDTO.class);
		}
	}

	@Override
	public List<RoundDTO> getRoundByEntityIdAndEntityType(Integer entityId, EntityType entityType) {
		List<RoundQO> listRoundQO = roundDAO.getRoundByEntityIdAndEntityType(entityId, entityType);

		if (listRoundQO == null) {
			throw new NotFoundException("Entity's Round with entityId=" + entityId + " was not found.");
		}

		List<RoundDTO> listRoundDTO = null;
		if (!CollectionUtils.isEmpty(listRoundQO)) {
			listRoundDTO = conversionService.convert(listRoundQO, RoundDTO.class);
		}
		return listRoundDTO;
	}

	@Transactional
	@Override
	public RoundDTO updateRoundByID(RoundDTO newRoundDTO) {
		Integer id = newRoundDTO.getId();
		if (id == null) {
			throw new ResourceInvalidException("Invalid request.");
		}
		RoundDTO dbRoundDTO = null;
		try {
			dbRoundDTO = getRoundByID(id);
		} catch (Exception e) {
			throw new NotFoundException("Round with id=" + id + " was not found.");
		}
		if (!isValidUpdate(newRoundDTO, dbRoundDTO)) {
			throw new ResourceInvalidException("Invalid request.");
		}

		RoundQO roundQO = this.conversionService.convert(newRoundDTO, RoundQO.class);
		roundQO.setModifiedUserId(authorityEvaluator.getCurrentUsername());
		Integer responseId = roundDAO.update(roundQO);
		return getRoundByID(responseId);
	}

	private boolean isValidUpdate(RoundDTO roundDTO, RoundDTO oldRoundDTO) {
		boolean isValid = true;

		isValid &= roundDTO.getEntityId() == oldRoundDTO.getEntityId()
				&& roundDTO.getEntityType().equals(oldRoundDTO.getEntityType());

		return isValid;
	}

	private boolean canDeleteRound(Integer id) {
		boolean canDelete = true;
		Integer numInterviews = 0;
		try {
			numInterviews = interviewService.getCountByRoundId(id);
		} catch (Exception e) {
			logger.error("exception={} on deleting round by id={}", e, id);
			throw new HDException("An error occurred on delete.");
		}
		if (numInterviews > 0)
			canDelete = false;

		return canDelete;
	}

	public void delete(Integer id) {
		if (canDeleteRound(id)) {
			roundDAO.delete(id);
		} else {
			throw new ResourceInvalidException("Invalid request.");
		}

	}

}
