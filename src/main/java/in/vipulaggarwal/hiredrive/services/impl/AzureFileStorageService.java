package in.vipulaggarwal.hiredrive.services.impl;


import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import in.vipulaggarwal.hiredrive.exceptions.AzureException;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import in.vipulaggarwal.hiredrive.services.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.InputStream;

public class AzureFileStorageService implements FileStorageService {
    private static final Logger logger = LoggerFactory.getLogger(AzureFileStorageService.class);

    private String containerName;
    private CloudBlobContainer cloudBlobContainer;

    @Autowired
    private CloudBlobClient cloudBlobClient;

    public AzureFileStorageService(String containerName) {

        this.containerName = containerName;
    }

    @PostConstruct
    private void createContainerIfNotExists() {
        try {

            // Get a reference to a container. (Name must be lower case.)
            this.cloudBlobContainer = cloudBlobClient.getContainerReference(containerName);
            // Create the container if it does not exist.
            cloudBlobContainer.createIfNotExists();
        } catch (Exception e) {
            logger.error("error creating container with exception={}", e);
            throw new HDException("Storage container init exception.");
        }
    }

    public void upload(InputStream inputStream, long fileSize, String path) {
        try {
            // Create a blob client.
            CloudBlockBlob blob = cloudBlobContainer.getBlockBlobReference(path);
            // Upload some text into the blob.
            blob.upload(inputStream, fileSize);

        } catch (Exception e) {
            // Output the stack trace.
            logger.error("Error Upload File to container.", e);
            throw new AzureException("Error in uploading file.");
        }

    }

    public String getStorageURIForContainer() {
        return cloudBlobContainer.getStorageUri().getPrimaryUri().toString();
    }
}
