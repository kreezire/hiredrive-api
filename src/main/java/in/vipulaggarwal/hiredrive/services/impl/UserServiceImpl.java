package in.vipulaggarwal.hiredrive.services.impl;

import in.vipulaggarwal.hiredrive.common.utils.Utils;
import in.vipulaggarwal.hiredrive.db.dao.BasicUserAuthDAO;
import in.vipulaggarwal.hiredrive.db.dao.BasicUserDAO;
import in.vipulaggarwal.hiredrive.db.dao.PasswordResetDAO;
import in.vipulaggarwal.hiredrive.db.qo.BasicUserAuthQO;
import in.vipulaggarwal.hiredrive.db.qo.BasicUserQO;
import in.vipulaggarwal.hiredrive.db.qo.PasswordResetQO;
import in.vipulaggarwal.hiredrive.dto.BasicUserAuthDTO;
import in.vipulaggarwal.hiredrive.dto.BasicUserDTO;
import in.vipulaggarwal.hiredrive.dto.impl.BasicUserDTOImpl;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.services.AbstractService;
import in.vipulaggarwal.hiredrive.services.EmailService;
import in.vipulaggarwal.hiredrive.services.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.security.Key;
import java.sql.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserServiceImpl extends AbstractService implements UserService {

	@Value("${hiredrive.reset.password.console.url}")
	private String resetPasswordConsoleUrl;

	@Autowired
	private BasicUserAuthDAO basicUserAuthDAO;

	@Autowired
	private BasicUserDAO basicUserDAO;

	@Autowired
	private PasswordResetDAO passwordResetDAO;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	EmailService emailService;

	public BasicUserDTO add(BasicUserAuthDTO basicUserAuthDTO) {

		basicUserAuthDTO.setPassword(passwordEncoder.encode(basicUserAuthDTO.getPassword()));

		BasicUserAuthQO basicUserAuthQO = conversionService.convert(basicUserAuthDTO, BasicUserAuthQO.class);
		basicUserAuthDAO.addUserAuth(basicUserAuthQO);

		// conversion from userAuthDTO to userQO
		BasicUserQO basicUserQO = conversionService.convert(basicUserAuthDTO, BasicUserQO.class);
		Integer id = basicUserDAO.addUser(basicUserQO); // adding user through DAO to DB
		// TODO get user and return here
		return getUserByUsername(basicUserQO.getEmailId());
	}

	public UserDetails getUserDetailsByUsername(String username) {
		BasicUserAuthQO basicUserAuthQO = basicUserAuthDAO.getAuthDetailsByUserId(username);
		if (basicUserAuthQO == null) {
			return null;
		}
		return conversionService.convert(basicUserAuthQO, UserDetails.class);
	}

	public BasicUserDTO getUserByUsername(String username) {
		BasicUserQO basicUserQO = basicUserDAO.getUserByUsername(username);
		return conversionService.convert(basicUserQO, BasicUserDTO.class);
	}

	public BasicUserDTO changePassword(BasicUserDTO basicUserDTO, String currentPassword, String newPassword) {
		String dbCurrentPassword = basicUserAuthDAO.getAuthDetailsByUserId(basicUserDTO.getEmailId()).getPassword();
		if (!passwordEncoder.matches(currentPassword, dbCurrentPassword)) {
			throw new ResourceInvalidException("Incorrect current Password!");
		}
		BasicUserAuthQO basicUserAuthQO = new BasicUserAuthQO();
		basicUserAuthQO.setEmailId(basicUserDTO.getEmailId());
		basicUserAuthQO.setPassword(passwordEncoder.encode(newPassword));
		basicUserAuthDAO.updateUserAuth(basicUserAuthQO);
		return getUserByUsername(basicUserAuthQO.getEmailId());
	}

	public BasicUserDTO forgotPassword(String emailId) {
		emailId = emailId.toLowerCase();
		BasicUserAuthQO existingUser = basicUserAuthDAO.getAuthDetailsByUserId(emailId);
		if (existingUser != null) {
			// Create token
			String jwt = Utils.createJWT(existingUser.getId().toString(), "hiringnote.com", existingUser.getEmailId(),
					0);

			// Save it
			PasswordResetQO passwordResetQO = new PasswordResetQO();
			passwordResetQO.setEmailId(emailId);
			passwordResetQO.setToken(jwt);
			if (passwordResetDAO.getByEmailId(emailId) != null) {
				passwordResetDAO.update(passwordResetQO);
			} else {
				passwordResetDAO.add(passwordResetQO);
			}

			// send the email
			String msg = "To complete the password reset process, please click here: " + resetPasswordConsoleUrl
					+ "?emailId=" + emailId + "&token=" + jwt;
			emailService.sendEMail(emailId, "Password Reset", msg);
		} else {
			throw new ResourceInvalidException("This email address does not exist!");
		}
		BasicUserDTO basicUserDTO = new BasicUserDTOImpl();
		basicUserDTO.setEmailId(existingUser.getEmailId());
		return basicUserDTO;
	}

	public void resetPassword(String emailId, String token, String newPassword, String confirmPassword) {
		emailId = emailId.toLowerCase();
		BasicUserAuthQO existingUser = basicUserAuthDAO.getAuthDetailsByUserId(emailId);
		if (existingUser != null) {

			// verify token
			PasswordResetQO passwordResetQO = passwordResetDAO.getByEmailId(emailId);
			if (passwordResetQO == null) {
				// forgot-password never called.
				throw new ResourceInvalidException("Illegal way of password reset!");
			}
			if (!passwordResetQO.getToken().equals(token)) {
				throw new ResourceInvalidException("Invalid authentication token!");
			}

			// validate token
			Utils.decodeJWT(token);

			// change password
			BasicUserAuthQO basicUserAuthQO = new BasicUserAuthQO();
			basicUserAuthQO.setEmailId(emailId);
			basicUserAuthQO.setPassword(passwordEncoder.encode(newPassword));
			basicUserAuthDAO.updateUserAuth(basicUserAuthQO);

			// delete token from db
			passwordResetDAO.deleteByEmailId(emailId);
		} else {
			throw new ResourceInvalidException("User does not exist!");
		}
	}

}
