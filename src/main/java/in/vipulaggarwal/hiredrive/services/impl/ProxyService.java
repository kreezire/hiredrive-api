package in.vipulaggarwal.hiredrive.services.impl;

import in.vipulaggarwal.hiredrive.common.utils.Utils;
import in.vipulaggarwal.hiredrive.exceptions.HDHttpException;
import in.vipulaggarwal.hiredrive.exceptions.HDHttpRetryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.annotation.Retryable;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@EnableRetry
public class ProxyService {
    private static final Logger logger = LoggerFactory.getLogger(ProxyService.class);

    private RestTemplate restTemplate;

    @PostConstruct
    private void init() {
        restTemplate = new RestTemplate();
    }

    @Retryable(maxAttempts = 3, backoff = @Backoff(delay = 30, multiplier = 2, maxDelay = 30000), exclude = {HDHttpRetryException.class})
    public <T> ResponseEntity<T> call(String url, HttpMethod httpMethod, Object request, Class<T> responseType, Map<String, String> queryParams, MediaType mediaType) throws HDHttpException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);
        if (CollectionUtils.isEmpty(queryParams)) {
            queryParams = new HashMap<>();
        }

        url = Utils.getURL(url, queryParams);

        HttpEntity<Object> entity = new HttpEntity<Object>((Object) request, headers);
        ResponseEntity<T> response = null;
        try {
            logger.info("Calling  proxy with params url - {}, http method - {}, headers - {}, request - {}", url, httpMethod, headers, request);
            response = restTemplate.exchange(url, httpMethod, entity, responseType);
            logger.info("Response status code = {} and response header = {}", response.getStatusCode().toString(), response.getHeaders());
        } catch (HttpStatusCodeException th) {
            logger.error("Http exception with status code = {}, header = {}, body = {}", th.getStatusCode().toString(), th.getResponseHeaders(), th.getResponseBodyAsString());
            if (th.getStatusCode().is4xxClientError()) {
                throw new HDHttpRetryException(th.getStatusCode(), th.getResponseHeaders(), th.getResponseBodyAsString());
            }
            throw new HDHttpException(th.getStatusCode(), th.getResponseHeaders(), th.getResponseBodyAsString());
        } catch (Throwable th) {
            logger.error("Error Message {} for error {} ", th.getMessage(), th);
            throw new HDHttpException(th.getMessage(), th);
        }
        return response;
    }
}
