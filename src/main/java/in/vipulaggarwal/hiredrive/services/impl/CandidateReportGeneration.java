package in.vipulaggarwal.hiredrive.services.impl;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TextAlignment;
import in.vipulaggarwal.hiredrive.dto.CandidateDTO;
import in.vipulaggarwal.hiredrive.dto.DriveDTO;
import in.vipulaggarwal.hiredrive.dto.InterviewDTO;
import in.vipulaggarwal.hiredrive.dto.RoundDTO;
import in.vipulaggarwal.hiredrive.enums.EntityType;
import in.vipulaggarwal.hiredrive.services.AbstractService;
import in.vipulaggarwal.hiredrive.services.CandidateService;
import in.vipulaggarwal.hiredrive.services.DriveService;
import in.vipulaggarwal.hiredrive.services.InterviewService;
import in.vipulaggarwal.hiredrive.services.ReportGenerationService;
import in.vipulaggarwal.hiredrive.services.RoundService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.NotFoundException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class CandidateReportGeneration extends AbstractService implements ReportGenerationService {
	@Autowired
	CandidateService candidateService;

	@Autowired
	DriveService driveService;

	@Autowired
	RoundService roundService;

	@Autowired
	InterviewService interviewService;

	DriveDTO drive = null;

	private void addTitle(Document document) throws IOException {
		PdfFont bold = PdfFontFactory.createFont(FontConstants.TIMES_BOLD);
		Text title = new Text("CANDIDATE REPORT").setFont(bold).setFontSize(20);
		Paragraph p = new Paragraph().add(title);
		p.setTextAlignment(TextAlignment.CENTER);
		document.add(p);
	}

	private void addBasicDetails(Document document, CandidateDTO candidate) {
		Paragraph subPara = new Paragraph("Candidate Details:").setFontSize(12).setBold();
//		float[] pointColumnWidths = { 150F, 150F };
		Table table = new Table(new float[2]).useAllAvailableWidth();
//		table.setHorizontalAlignment(HorizontalAlignment.CENTER);
//		table.setMarginTop(0);
//      table.setMarginBottom(0);
		table.addCell(new Cell().add(new Paragraph().add("Name:").setFontSize(8).setBold()));
		table.addCell(new Cell().add(new Paragraph().add(candidate.getName()).setFontSize(8)));
		if (candidate.getPhone() != null) {
			table.addCell(new Cell().add(new Paragraph().add("Contact no:").setFontSize(8).setBold()));
			table.addCell(new Cell().add(new Paragraph().add(candidate.getPhone()).setFontSize(8)));
		}
		if (candidate.getEmailId() != null) {
			table.addCell(new Cell().add(new Paragraph().add("E-Mail:").setFontSize(8).setBold()));
			table.addCell(new Cell().add(new Paragraph().add(candidate.getEmailId()).setFontSize(8)));
		}
		table.addCell(new Cell().add(new Paragraph().add("Status:").setFontSize(8).setBold()));
		table.addCell(new Cell().add(new Paragraph().add(candidate.getStatus().toString()).setFontSize(8)));

		if (drive != null) {
			table.addCell(new Cell().add(new Paragraph().add("Drive Name:").setFontSize(8).setBold()));
			table.addCell(new Cell().add(new Paragraph().add(drive.getName()).setFontSize(8)));

		}

		document.add(subPara);
		document.add(table);
	}

	private InterviewDTO getInterviewFromListByRoundID(List<InterviewDTO> interviews, int id) {
		if (interviews != null) {
			for (InterviewDTO interview : interviews) {
				if (interview.getRoundId() == id) {
					return interview;
				}
			}
		}
		return null;
	}

	private void addInterviewDetails(Document document, CandidateDTO candidate) {
		Paragraph subPara = new Paragraph("Interview Details:").setFontSize(12).setBold();
		Table table = new Table(new float[6]).useAllAvailableWidth();
		table.addCell(new Cell()
				.add(new Paragraph().add("S.No.").setFontSize(8).setBold().setTextAlignment(TextAlignment.CENTER)));
		table.addCell(new Cell()
				.add(new Paragraph().add("Round").setFontSize(8).setBold().setTextAlignment(TextAlignment.CENTER)));
		table.addCell(new Cell().add(
				new Paragraph().add("Interviewer").setFontSize(8).setBold().setTextAlignment(TextAlignment.CENTER)));
		table.addCell(new Cell()
				.add(new Paragraph().add("Duration").setFontSize(8).setBold().setTextAlignment(TextAlignment.CENTER)));
		table.addCell(new Cell()
				.add(new Paragraph().add("Reviews").setFontSize(8).setBold().setTextAlignment(TextAlignment.CENTER)));
		table.addCell(new Cell()
				.add(new Paragraph().add("Rating").setFontSize(8).setBold().setTextAlignment(TextAlignment.CENTER)));

		if (drive != null) {
			addInterviewDataToTable(candidate, table, drive.getId(), EntityType.DRIVE);
		}
		addInterviewDataToTable(candidate, table, candidate.getId(), EntityType.CANDIDATE);
		document.add(subPara);
		document.add(table);
	}

	private void addInterviewDataToTable(CandidateDTO candidate, Table table, Integer entityId, EntityType entityType) {
		List<RoundDTO> rounds = roundService.getRoundByEntityIdAndEntityType(entityId, entityType);
		if (rounds != null) {
			List<InterviewDTO> interviews = interviewService.getInterviewByCandidateId(candidate.getId());
			Integer sno = 1;
			for (RoundDTO round : rounds) {
				String roundName = round.getName();
				addRow(table, roundName, sno);
				sno++;
				InterviewDTO interview = getInterviewFromListByRoundID(interviews, round.getId());
				if (interview != null) {
					table.addCell(new Cell().add(new Paragraph().add(interview.getInterviewer()).setFontSize(8)
							.setTextAlignment(TextAlignment.CENTER)));
					String duration = interview.getDuration() == null ? "NA" : interview.getDuration().toString();
					table.addCell(new Cell()
							.add(new Paragraph().add(duration).setFontSize(8).setTextAlignment(TextAlignment.CENTER)));
					String feedback = "NA";
					if (interview.getFeedback() != null) {
						try {
							JSONObject feedbackRaw = new JSONObject(interview.getFeedback());
							JSONArray ja = (JSONArray) feedbackRaw.get("blocks");
							feedback = (String) ja.getJSONObject(0).get("text");
						} catch (JSONException err) {
							feedback = "NA";
						}
					}
					table.addCell(new Cell()
							.add(new Paragraph().add(feedback).setFontSize(8).setTextAlignment(TextAlignment.CENTER)));

					String rating = interview.getRating() == null ? "NA" : interview.getRating().toString();

					table.addCell(new Cell()
							.add(new Paragraph().add(rating).setFontSize(8).setTextAlignment(TextAlignment.CENTER)));
				} else {
					table.addCell(new Cell()
							.add(new Paragraph().add("NA").setFontSize(8).setTextAlignment(TextAlignment.CENTER)));
					table.addCell(new Cell()
							.add(new Paragraph().add("NA").setFontSize(8).setTextAlignment(TextAlignment.CENTER)));
					table.addCell(new Cell()
							.add(new Paragraph().add("NA").setFontSize(8).setTextAlignment(TextAlignment.CENTER)));
					table.addCell(new Cell()
							.add(new Paragraph().add("NA").setFontSize(8).setTextAlignment(TextAlignment.CENTER)));
				}
			}
		}
	}

	private void addRow(Table table, String roundName, Integer sno) {
		table.addCell(new Cell()
				.add(new Paragraph().add(sno.toString()).setFontSize(8).setTextAlignment(TextAlignment.CENTER)));

		table.addCell(
				new Cell().add(new Paragraph().add(roundName).setFontSize(8).setTextAlignment(TextAlignment.CENTER)));
	}

	public void genReport(Integer id) {
		CandidateDTO candidate = candidateService.getCandidateById(id);
		String dest = "sample.pdf";
		PdfWriter writer;
		try {
			writer = new PdfWriter(dest);
			PdfDocument pdfDoc = new PdfDocument(writer); // Creating a PdfDocument
			pdfDoc.addNewPage(); // Adding a new page
			Document document = new Document(pdfDoc);// Creating a Document
			if (candidate.getDriveId() != null) {
				try {
					drive = driveService.getDriveByID(candidate.getDriveId());
				} catch (NotFoundException e) {
					drive = null;
				}
			}
			addTitle(document);
			addBasicDetails(document, candidate);
			addInterviewDetails(document, candidate);

			document.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
