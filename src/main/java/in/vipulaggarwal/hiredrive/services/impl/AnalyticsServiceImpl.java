package in.vipulaggarwal.hiredrive.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import in.vipulaggarwal.hiredrive.db.dao.CandidateDAO;
import in.vipulaggarwal.hiredrive.db.dao.InterviewDAO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewerAnalyticsQO;
import in.vipulaggarwal.hiredrive.db.qo.RoundAnalyticsQO;
import in.vipulaggarwal.hiredrive.db.qo.TalentPartnerAnalyticsQO;
import in.vipulaggarwal.hiredrive.dto.BarChartResponseDTO;
import in.vipulaggarwal.hiredrive.dto.GroupedBarChartResponseDTO;
import in.vipulaggarwal.hiredrive.dto.RoundDTO;
import in.vipulaggarwal.hiredrive.enums.EntityType;
import in.vipulaggarwal.hiredrive.services.AbstractService;
import in.vipulaggarwal.hiredrive.services.AnalyticsService;
import in.vipulaggarwal.hiredrive.services.RoundService;

public class AnalyticsServiceImpl extends AbstractService implements AnalyticsService {

	@Autowired
	private RoundService roundService;

	@Autowired
	private InterviewDAO interviewDAO;

	@Autowired
	private CandidateDAO candidateDAO;

	public BarChartResponseDTO<String> getRoundStatsForDrive(Integer driveId) {
		BarChartResponseDTO<String> barChartResponseDTO = null;
		List<RoundDTO> listRoundDTOs = roundService.getRoundByEntityIdAndEntityType(driveId, EntityType.DRIVE);
		if (!CollectionUtils.isEmpty(listRoundDTOs)) {
			Map<Integer, String> roundIdToNameMap = new HashMap<>();
			Set<Integer> listRoundId = new HashSet<>();
			for (RoundDTO round : listRoundDTOs) {
				roundIdToNameMap.put(round.getId(), round.getName());
				listRoundId.add(round.getId());
			}

			List<RoundAnalyticsQO> listRoundAnalyticsQOs = interviewDAO.getRoundAnalytics(listRoundId);
			List<String> xData = new ArrayList<>();
			List<Integer> yData = new ArrayList<>();

			for (RoundAnalyticsQO roundAnalyticsQO : listRoundAnalyticsQOs) {
				xData.add(roundIdToNameMap.get(roundAnalyticsQO.getRoundId()));
				yData.add(roundAnalyticsQO.getCount());
				roundIdToNameMap.remove(roundAnalyticsQO.getRoundId());
			}

			for (Map.Entry<Integer, String> entry : roundIdToNameMap.entrySet()) {
				xData.add(entry.getValue());
				yData.add(0);
			}

			barChartResponseDTO = new BarChartResponseDTO<>();
			barChartResponseDTO.setxData(xData);
			barChartResponseDTO.setyData(yData);
		}
		return barChartResponseDTO;
	}

	public BarChartResponseDTO<String> getInterviewerStatsForDrive(Integer driveId) {
		BarChartResponseDTO<String> barChartResponseDTO = null;
		List<RoundDTO> listRoundDTOs = roundService.getRoundByEntityIdAndEntityType(driveId, EntityType.DRIVE);

		if (!CollectionUtils.isEmpty(listRoundDTOs)) {
			Set<Integer> listRoundId = new HashSet<>();
			for (RoundDTO round : listRoundDTOs) {
				listRoundId.add(round.getId());
			}

			List<InterviewerAnalyticsQO> listRoundAnalyticsQOs = interviewDAO.getInterviewerAnalytics(listRoundId);

			if (!CollectionUtils.isEmpty(listRoundAnalyticsQOs)) {
				List<String> xData = new ArrayList<>();
				List<Integer> yData = new ArrayList<>();

				for (InterviewerAnalyticsQO interviewerAnalyticsQO : listRoundAnalyticsQOs) {
					xData.add(interviewerAnalyticsQO.getInterviewer());
					yData.add(interviewerAnalyticsQO.getCount());
				}
				barChartResponseDTO = new BarChartResponseDTO<>();
				barChartResponseDTO.setxData(xData);
				barChartResponseDTO.setyData(yData);
			}
		}

		return barChartResponseDTO;
	}

	public GroupedBarChartResponseDTO<String, String> getTalentPartnerStatsForDrive(Integer driveId) {
		List<TalentPartnerAnalyticsQO> listTalentPartnerAnalyticsQOs = candidateDAO
				.getTalentPartnerAnalyticsForDrive(driveId);
		GroupedBarChartResponseDTO<String, String> groupedBarChartResponseDTO = null;
		if (!CollectionUtils.isEmpty(listTalentPartnerAnalyticsQOs)) {
			groupedBarChartResponseDTO = new GroupedBarChartResponseDTO<>();

			List<String> xData = new ArrayList<>();
			List<String> subGroup = new ArrayList<>(Arrays.asList("Total", "Selected"));
			List<List<Integer>> yData = new ArrayList<>();

			for (TalentPartnerAnalyticsQO talentPartnerAnalyticsQO : listTalentPartnerAnalyticsQOs) {
				xData.add(talentPartnerAnalyticsQO.getTalentPartner() == null ? "Unassigned"
						: talentPartnerAnalyticsQO.getTalentPartner());
				yData.add(new ArrayList<Integer>(
						Arrays.asList(talentPartnerAnalyticsQO.getTotal(), talentPartnerAnalyticsQO.getSelected())));
			}

			groupedBarChartResponseDTO.setxData(xData);
			groupedBarChartResponseDTO.setSubGroup(subGroup);
			groupedBarChartResponseDTO.setyData(yData);
		}

		return groupedBarChartResponseDTO;
	}
}
