package in.vipulaggarwal.hiredrive.services.impl;

import in.vipulaggarwal.hiredrive.common.utils.ClockUtils;
import in.vipulaggarwal.hiredrive.common.utils.InterviewHelper;
import in.vipulaggarwal.hiredrive.db.dao.InterviewDAO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewCandidateRoundQO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewQO;
import in.vipulaggarwal.hiredrive.dto.CandidateDTO;
import in.vipulaggarwal.hiredrive.dto.DriveDTO;
import in.vipulaggarwal.hiredrive.dto.InterviewCandidateRoundDTO;
import in.vipulaggarwal.hiredrive.dto.InterviewDTO;
import in.vipulaggarwal.hiredrive.dto.RoundDTO;
import in.vipulaggarwal.hiredrive.enums.CandidateStatus;
import in.vipulaggarwal.hiredrive.enums.DriveStatus;
import in.vipulaggarwal.hiredrive.enums.InterviewStatus;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.services.AbstractService;
import in.vipulaggarwal.hiredrive.services.CandidateService;
import in.vipulaggarwal.hiredrive.services.DriveService;
import in.vipulaggarwal.hiredrive.services.InterviewService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;

public class InterviewServiceImpl extends AbstractService implements InterviewService {

	private static final Logger logger = LoggerFactory.getLogger(InterviewServiceImpl.class);

	@Autowired
	InterviewDAO interviewDAO;

	@Autowired
	private DriveService driveService;

	@Autowired
	private CandidateService candidateService;

	public InterviewDTO addInterview(InterviewDTO interviewDTO) {
		CandidateDTO candidateDTO = candidateService.getCandidateById(interviewDTO.getCandidateId());
		DriveDTO driveDTO = null;
		if (candidateDTO != null && candidateDTO.getDriveId() != null) {
			driveDTO = driveService.getDriveByID(candidateDTO.getDriveId());
		}
		if (driveDTO != null && driveDTO.getStatus() == DriveStatus.CLOSE) {
			logger.error("Can't POST interviewDTO [Drive is CLOSE]={}", interviewDTO);
			throw new ResourceInvalidException("Seems like Drive is currently CLOSE!");
		}
		interviewDTO.setStatus(InterviewStatus.PENDING);
		InterviewQO interviewQO = conversionService.convert(interviewDTO, InterviewQO.class);
		interviewQO.setCreatedUserId(authorityEvaluator.getCurrentUsername());
		Integer id = interviewDAO.add(interviewQO);

		return getById(id);
	}

	public InterviewDTO getById(Integer id) {
		InterviewQO interviewQO = interviewDAO.get(id);
		return conversionService.convert(interviewQO, InterviewDTO.class);
	}

	private List<InterviewQO> getInterviewsByCandidateIdAndStatus(Integer candidateId,
			InterviewStatus interviewStatus) {
		List<InterviewQO> list = interviewDAO.getByCandidateIdAndStatus(candidateId, interviewStatus.toString());
		if (list == null || list.size() == 0) {
			throw new NotFoundException("Interview not found.");
		}
		return list;
	}

	private boolean isAnyRoundOngoing(Integer candidateId) {
		boolean result = false;
		List<InterviewQO> ongoingInterviews = null;
		try {
			ongoingInterviews = getInterviewsByCandidateIdAndStatus(candidateId, InterviewStatus.ONGOING);
		} catch (NotFoundException e) {
			result = false;
		}
		if (ongoingInterviews != null) {
			for (InterviewQO curr : ongoingInterviews) {
				if (InterviewStatus.ONGOING.toString().equals(curr.getStatus())) {
					result = true;
					break;
				}
			}
		}
		return result;
	}

	private Integer updateInterviewQO(InterviewQO interviewQO) {
		interviewQO.setModifiedUserId(authorityEvaluator.getCurrentUsername());
		return interviewDAO.update(interviewQO);
	}

	@Transactional
	public InterviewDTO update(InterviewDTO interviewDTO) {
		InterviewDTO dbInterviewDTO = getById(interviewDTO.getId());
		CandidateDTO candidateDTO = candidateService.getCandidateById(interviewDTO.getCandidateId());
		DriveDTO driveDTO = null;
		if (candidateDTO != null && candidateDTO.getDriveId() != null) {
			driveDTO = driveService.getDriveByID(candidateDTO.getDriveId());
		}

		if (driveDTO != null && driveDTO.getStatus() == DriveStatus.CLOSE) {
			logger.error("Can't PUT interviewDTO [Drive is CLOSE]={}", interviewDTO);
			throw new ResourceInvalidException("Seems like Drive is currently CLOSE!");
		}

		if (!InterviewHelper.isValidForUpdate(dbInterviewDTO, interviewDTO)) {
			logger.error("can't update interviewDTO={} to new interviewDTO={}", dbInterviewDTO, interviewDTO);
			throw new ResourceInvalidException("Invalid update for interview.");
		}

		if (InterviewHelper.isValidInterviewerStartUpdate(interviewDTO.getInterviewer(), interviewDTO.getStatus(),
				dbInterviewDTO.getStatus())) {

			if (isAnyRoundOngoing(interviewDTO.getCandidateId())) {
				logger.error("another round already in progress for update interviewDTO={}", interviewDTO);
				throw new ResourceInvalidException("Another round is already in progress!");
			}
			interviewDTO.setStatus(InterviewStatus.ONGOING);
			interviewDTO.setStartTime(ClockUtils.getUnixMiliSeconds());
			candidateService.updateCandidateStatus(interviewDTO.getCandidateId(), CandidateStatus.IN_PROGRESS);

		}

		if (InterviewHelper.isValidInterviewerSubmitUpdate(interviewDTO.getInterviewer(),
				dbInterviewDTO.getInterviewer(), interviewDTO.getStatus(), dbInterviewDTO.getStatus())) {
			if (StringUtils.isEmpty(interviewDTO.getFeedback())
					|| !InterviewHelper.isValidRating(interviewDTO.getRating())) {
				logger.error("invalid feedback or rating for interviewDTO={}", interviewDTO);
				throw new ResourceInvalidException("Feedback or rating missing.");
			}
			interviewDTO.setStatus(InterviewStatus.DONE);
			interviewDTO.setSubmitTime(ClockUtils.getUnixMiliSeconds());
			candidateService.updateCandidateStatus(interviewDTO.getCandidateId(), CandidateStatus.WAITING);
		}

		InterviewQO interviewQO = conversionService.convert(interviewDTO, InterviewQO.class);
		updateInterviewQO(interviewQO);
		return getById(interviewDTO.getId());
	}

	public InterviewDTO getByCandidateIdAndRoundId(Integer candidateId, Integer roundId) {
		InterviewQO interviewQO = interviewDAO.getByCandidateIdAndRoundId(candidateId, roundId);
		InterviewDTO interviewDTO = null;
		if (interviewQO == null) {
			logger.error("Interview record for candidateId={} and roundId={} not found", candidateId, roundId);
			interviewDTO = InterviewHelper.getDummyInterview(roundId, candidateId);
		} else {
			interviewDTO = conversionService.convert(interviewQO, InterviewDTO.class);
		}
		return interviewDTO;
	}

	public Integer getCountByRoundId(Integer roundId) {
		return interviewDAO.getCountByRoundId(roundId);
	}

	@Override
	public List<InterviewDTO> getInterviewByCandidateId(Integer candidateId) {
		List<InterviewQO> list = interviewDAO.getInterviewByCandidateId(candidateId);
		List<InterviewDTO> interviewDTOs = null;
		if (list == null || list.size() == 0) {
			logger.info("Interviews not found for candidateId={}.", candidateId);
			throw new ResourceInvalidException("No interview found for the candidate.");
		} else {
			interviewDTOs = conversionService.convert(list, InterviewDTO.class);
		}
		return interviewDTOs;
	}

	public List<InterviewDTO> getInterviewAssignedToMe() {
		List<InterviewQO> list = interviewDAO.getInterviewAssignedToMe(authorityEvaluator.getCurrentUsername());
		List<InterviewDTO> interviewDTOs = null;
		if (list == null || list.size() == 0) {
			logger.info("No Interview assigned to user={}.", authorityEvaluator.getCurrentUsername());
		} else {
			interviewDTOs = conversionService.convert(list, InterviewDTO.class);
		}
		return interviewDTOs;
	}

	public List<InterviewCandidateRoundDTO> getInterviewCandidateRoundDetailsAssignedToMe() {
		List<InterviewCandidateRoundQO> list = interviewDAO
				.getInterviewCandidateRoundDetailsAssignedToMe(authorityEvaluator.getCurrentUsername());
		List<InterviewCandidateRoundDTO> interviewCandidateRoundDTOs = null;
		if (list == null || list.size() == 0) {
			logger.info("No Interview assigned to user={}.", authorityEvaluator.getCurrentUsername());
		} else {
			interviewCandidateRoundDTOs = conversionService.convert(list, InterviewCandidateRoundDTO.class);
		}
		return interviewCandidateRoundDTOs;
	}

	public boolean isAnyRoundOngoingInDrive(Integer driveId) {
		List<CandidateDTO> candidateList = candidateService.getCandidateListByDriveId(driveId);
		if (candidateList != null) {
			for (CandidateDTO curr : candidateList) {
				if (isAnyRoundOngoing(curr.getId())) {
					return true;
				}
			}
		}
		return false;
	}

	public List<InterviewDTO> getByDriveIdAndInterviewer(Integer driveId, String interviewer) {
		List<InterviewQO> interviewQOs = interviewDAO.getInterviewsByDriveIdAndInterviewer(driveId, interviewer);
		List<InterviewDTO> interviewDTOs = null;
		if (!CollectionUtils.isEmpty(interviewQOs)) {
			interviewDTOs = conversionService.convert(interviewQOs, InterviewDTO.class);
		}
		return interviewDTOs;
	}
}
