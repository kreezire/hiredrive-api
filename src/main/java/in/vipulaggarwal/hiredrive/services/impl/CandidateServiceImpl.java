package in.vipulaggarwal.hiredrive.services.impl;

import in.vipulaggarwal.hiredrive.common.ConversionService;
import in.vipulaggarwal.hiredrive.common.utils.CandidateHelper;
import in.vipulaggarwal.hiredrive.common.utils.ObjUtils;
import in.vipulaggarwal.hiredrive.common.utils.ResumeHelper;
import in.vipulaggarwal.hiredrive.db.dao.CandidateDAO;
import in.vipulaggarwal.hiredrive.db.qo.CandidateQO;
import in.vipulaggarwal.hiredrive.dto.CandidateDTO;
import in.vipulaggarwal.hiredrive.dto.DriveDTO;
import in.vipulaggarwal.hiredrive.enums.CandidateStatus;
import in.vipulaggarwal.hiredrive.enums.DriveStatus;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.services.AbstractService;
import in.vipulaggarwal.hiredrive.services.CandidateService;
import in.vipulaggarwal.hiredrive.services.DriveService;
import in.vipulaggarwal.hiredrive.services.FileStorageService;
import in.vipulaggarwal.hiredrive.services.ResumeParser;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CandidateServiceImpl extends AbstractService implements CandidateService {

	private static final Logger logger = LoggerFactory.getLogger(CandidateServiceImpl.class);

	@Autowired
	ConversionService conversionService;

	@Autowired
	CandidateDAO candidateDAO;

	@Autowired
	FileStorageService azureFileStorageService;

	@Autowired
	ResumeParser resumeParser;

	@Autowired
	private DriveService driveService;

	@Transactional
	public CandidateDTO addCandidate(InputStream uploadedInputStream, FormDataContentDisposition fileDetails,
			CandidateQO candidateQO) {
		Integer id = -1;
		Boolean validFile = ObjUtils.validFile(fileDetails);
		Boolean validCandidateQO = ObjUtils.validForAddCandidateQO(candidateQO);

		if (!validFile && validCandidateQO) {
			id = addCandidateQO(candidateQO);
			candidateQO.setId(id);
		} else if (validFile && !validCandidateQO) {
			candidateQO = new CandidateQO();
			candidateQO.setResumePath(fileDetails.getFileName().replace(' ', '_'));
			id = addCandidateQO(candidateQO);
			candidateQO.setId(id);
			String path = ResumeHelper.getResumePath(id, candidateQO.getResumePath());
			uploadResume(uploadedInputStream, fileDetails.getSize(), path);
			updateCandidateDetailsFromResume(candidateQO, getFileStorageURL(path));
		} else if (validFile && validCandidateQO) {
			candidateQO.setResumePath(fileDetails.getFileName().replace(' ', '_'));
			id = addCandidateQO(candidateQO);
			candidateQO.setId(id);
			String path = ResumeHelper.getResumePath(id, candidateQO.getResumePath());
			uploadResume(uploadedInputStream, fileDetails.getSize(), path);
		} else {
			throw new ResourceInvalidException("This should never happen.");
		}
		return this.getCandidateById(id);
	}

	private Integer addCandidateQO(CandidateQO candidateQO) {
		candidateQO.setStatus(CandidateStatus.NEW.toString());
		candidateQO.setCreatedUserId(authorityEvaluator.getCurrentUsername());
		return candidateDAO.add(candidateQO);
	}

	private void updateCandidateQO(CandidateQO candidateQO) {
		candidateQO.setModifiedUserId(authorityEvaluator.getCurrentUsername());
		candidateDAO.update(candidateQO);
	}

	private CandidateQO getCandidateQOById(Integer id) {
		CandidateQO candidateQO = candidateDAO.get(id);
		if (candidateQO == null) {
			throw new NotFoundException("Candidate with id=" + id + " was not found.");
		}
		return candidateQO;
	}

	public CandidateDTO getCandidateById(Integer id) {
		CandidateQO candidateQO = getCandidateQOById(id);

		CandidateDTO candidateDTO = this.conversionService.convert(candidateQO, CandidateDTO.class);
		return candidateDTO;
	}

	private CandidateQO updateCandidateDetailsFromResume(CandidateQO candidateQO, String fileStorageURL) {
		try {
			resumeParser.fillCandidateQOFromResume(candidateQO, fileStorageURL);
			// TODO Validate
			updateCandidateQO(candidateQO);
		} catch (HDException e) {
			logger.error("Exception in parsing resume={}", e);
		}
		return candidateQO;
	}

	private void uploadResume(InputStream uploadedInputStream, long size, String path) {
		azureFileStorageService.upload(uploadedInputStream, size, path);
	}

	private String getFileStorageURL(String path) {
		return azureFileStorageService.getStorageURIForContainer() + "/" + path;
	}

	public void updateCandidateStatus(Integer id, CandidateStatus candidateStatus) {
		CandidateQO candidateQO = getCandidateQOById(id);

		if (CandidateHelper.isValidStatusTransition(CandidateStatus.fromString(candidateQO.getStatus()),
				candidateStatus)) {
			candidateQO.setStatus(candidateStatus.toString());
			updateCandidateQO(candidateQO);
		} else {
			logger.error("cannot change status of candidate={} from={} to={}", id, candidateQO.getStatus(),
					candidateStatus.toString());
			throw new ResourceInvalidException("Candidate status change not allowed.");
		}

	}

	public CandidateDTO updateCandidate(InputStream uploadedInputStream, FormDataContentDisposition fileDetails,
			CandidateDTO candidateDTO) {
		CandidateQO oldCandidateQO = getCandidateQOById(candidateDTO.getId());
		//DriveDTO driveDTO = driveService.getDriveByID(candidateDTO.getDriveId());

		if (oldCandidateQO.getStatus() != candidateDTO.getStatus().toString()
				&& (!CandidateHelper.isValidStatusTransition(CandidateStatus.fromString(oldCandidateQO.getStatus()),
						candidateDTO.getStatus()))) {
			logger.error("cannot change status of candidate={} from={} to={}", candidateDTO.getId(),
					oldCandidateQO.getStatus(), candidateDTO.getStatus().toString());
			throw new ResourceInvalidException("Invalid candidate status change from " + oldCandidateQO.getStatus()
					+ " to " + candidateDTO.getStatus() + "!");
		}

		CandidateQO candidateQO = conversionService.convert(candidateDTO, CandidateQO.class);
		if (ObjUtils.validFile(fileDetails)) {
			candidateQO.setResumePath(fileDetails.getFileName().replace(' ', '_'));
			String path = ResumeHelper.getResumePath(candidateQO.getId(), candidateQO.getResumePath());
			uploadResume(uploadedInputStream, fileDetails.getSize(), path);

		} else {
			candidateQO.setResumePath(oldCandidateQO.getResumePath());
		}
		updateCandidateQO(candidateQO);
		return getCandidateById(candidateDTO.getId());
	}

	public List<CandidateDTO> getCandidateListByDriveId(Integer driveId) {
		List<CandidateQO> listCandidateQO = candidateDAO.getCandidateListByDriveId(driveId);
		if (listCandidateQO == null) {
			throw new NotFoundException("Candidates with DRIVE_ID=" + driveId + " not found.");
		}
		List<CandidateDTO> listCandidateDTO = null;
		if (!CollectionUtils.isEmpty(listCandidateQO)) {
			listCandidateDTO = conversionService.convert(listCandidateQO, CandidateDTO.class);
		}
		return listCandidateDTO;
	}

	public List<CandidateDTO> getMyCandidates() {
		List<CandidateQO> listCandidateQO = candidateDAO
				.getNonDriveCandidatesByCreatedBy(authorityEvaluator.getCurrentUsername());
		List<CandidateDTO> candidateDTOList = new ArrayList<>();
		if (listCandidateQO != null && listCandidateQO.size() > 0)
			candidateDTOList = conversionService.convert(listCandidateQO, CandidateDTO.class);
		return candidateDTOList;
	}
}
