package in.vipulaggarwal.hiredrive.services.impl;

import in.vipulaggarwal.hiredrive.db.dao.InterviewerDAO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewQO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewerQO;
import in.vipulaggarwal.hiredrive.dto.InterviewDTO;
import in.vipulaggarwal.hiredrive.dto.InterviewerDTO;
import in.vipulaggarwal.hiredrive.enums.DriveStatus;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.services.AbstractService;
import in.vipulaggarwal.hiredrive.services.InterviewService;
import in.vipulaggarwal.hiredrive.services.InterviewerService;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.NotFoundException;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

public class InterviewerServiceImpl extends AbstractService implements InterviewerService {

	private static final Logger logger = LoggerFactory.getLogger(InterviewerServiceImpl.class);

	@Autowired
	InterviewerDAO interviewerDAO;

	@Autowired
	InterviewService interviewService;

	public InterviewerDTO addInterviewer(InterviewerDTO interviewerDTO) {
		InterviewerQO interviewerQO = interviewerDAO.getByDriveIdAndEmailId(interviewerDTO.getDriveId(),
				interviewerDTO.getEmailId());
		if (interviewerQO != null) {
			logger.info("interviewer={} already exists in DB.", interviewerDTO);
			throw new ResourceInvalidException("Interviewer is already added to this drive.");
		}
		interviewerQO = conversionService.convert(interviewerDTO, InterviewerQO.class);
		interviewerQO.setCreatedUserId(authorityEvaluator.getCurrentUsername());
		Integer id = interviewerDAO.add(interviewerQO);
		return getById(id);
	}

	public InterviewerDTO getById(Integer id) {
		InterviewerQO interviewerQO = interviewerDAO.get(id);
		return conversionService.convert(interviewerQO, InterviewerDTO.class);
	}

	public InterviewerDTO update(InterviewerDTO interviewerDTO) {
		// InterviewerDTO dbInterviewerDTO = getById(interviewerDTO.getId());
		InterviewerQO interviewerQO = conversionService.convert(interviewerDTO, InterviewerQO.class);
		interviewerQO.setModifiedUserId(authorityEvaluator.getCurrentUsername());
		interviewerDAO.update(interviewerQO);
		return getById(interviewerDTO.getId());
	}

	public void delete(Integer id) {
		InterviewerDTO interviewer = getById(id);
		if (interviewer == null) {
			logger.info("Cannot delete interviewer id={}, not present in db.", id);
			throw new NotFoundException("Invalid interviewer id. Cannot delete interviewer.");
		}
		List<InterviewDTO> interviewDTOs = interviewService.getByDriveIdAndInterviewer(interviewer.getDriveId(),
				interviewer.getEmailId());
		if (!CollectionUtils.isEmpty(interviewDTOs)) {
			logger.info("Cannot delete interviewer id={}. Interviews exist.", id);
			throw new ResourceInvalidException("Cannot delete interviewer with assigned interviews.");
		}
		interviewerDAO.delete(id);
	}

	public List<InterviewerDTO> getInterviewersByDriveId(@NotNull Integer driveId) {
		List<InterviewerQO> list = interviewerDAO.getInterviewersByDriveId(driveId);
		List<InterviewerDTO> interviewerDTOs = null;
		if (list == null || list.size() == 0) {
			logger.info("Interviewers not found for driveId={}.", driveId);
			throw new ResourceInvalidException("No interviewers found for the drive.");
		} else {
			interviewerDTOs = conversionService.convert(list, InterviewerDTO.class);
		}
		return interviewerDTOs;
	}

}
