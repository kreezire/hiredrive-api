package in.vipulaggarwal.hiredrive.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;

import in.vipulaggarwal.hiredrive.services.EmailService;

public class EmailServiceImpl implements EmailService {

	@Autowired
	private JavaMailSender javaMailSender;
	
	private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);
	
	private static final String EMAIL_FROM = "Hiring Note <noreply@hiringnote.com>";
	
	
	@Async
	public void sendEMail(String to, String subject, String text) {	        
		SimpleMailMessage message = new SimpleMailMessage(); 
		message.setFrom(EMAIL_FROM);
		message.setTo(to); 
		message.setSubject(subject); 
		message.setText(text);
		logger.info("Sending email message={}", message);
		try {
			javaMailSender.send(message);
		}
		catch (MailException e) {
			logger.error("exception={} sending email={}", e, message);
		}
	}
}
