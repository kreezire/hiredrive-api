package in.vipulaggarwal.hiredrive.services.impl;

import in.vipulaggarwal.hiredrive.db.dao.DriveDAO;
import in.vipulaggarwal.hiredrive.db.qo.DriveQO;
import in.vipulaggarwal.hiredrive.dto.DriveDTO;
import in.vipulaggarwal.hiredrive.enums.DriveStatus;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.services.AbstractService;
import in.vipulaggarwal.hiredrive.services.DriveService;
import in.vipulaggarwal.hiredrive.services.InterviewService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;

public class DriveServiceImpl extends AbstractService implements DriveService {

	private static final Logger logger = LoggerFactory.getLogger(DriveServiceImpl.class);

	@Autowired
	private DriveDAO driveDAO;

	@Autowired
	private InterviewService interviewService;

	@Transactional
	public DriveDTO addDrive(DriveDTO driveDTO) {
		// conversion from driveDTO to driveQO
		DriveQO driveQO = conversionService.convert(driveDTO, DriveQO.class);
		driveQO.setCreatedUserId(authorityEvaluator.getCurrentUsername());
		Integer id = driveDAO.addDrive(driveQO); // adding drive through DAO to DB
		return this.getDriveByID(id);
	}

	public DriveDTO getDriveByID(Integer id) {
		DriveQO driveQO = driveDAO.get(id);

		if (driveQO == null) {
			throw new NotFoundException("Drive with id=" + id + " was not found.");
		} else {
			DriveDTO driveDTO = this.conversionService.convert(driveQO, DriveDTO.class);
			return driveDTO;
		}
	}

	private DriveQO getDriveQOById(Integer id) {
		DriveQO driveQO = driveDAO.get(id);
		if (driveQO == null) {
			throw new NotFoundException("Drive with id=" + id + " was not found.");
		}
		return driveQO;
	}

	private void updateDriveQO(DriveQO driveQO) {
		driveQO.setModifiedUserId(authorityEvaluator.getCurrentUsername());
		driveDAO.update(driveQO);
	}

	@Override
	public DriveDTO updateDrive(DriveDTO driveDTO) {
		DriveDTO oldDriveDTO = getDriveByID(driveDTO.getId());
		if (oldDriveDTO.getStatus() == DriveStatus.OPEN && driveDTO.getStatus() == DriveStatus.CLOSE
				&& interviewService.isAnyRoundOngoingInDrive(driveDTO.getId())) {
			logger.error("cannot change status of drive={} from={} to={}", driveDTO.getId(), oldDriveDTO.getStatus(),
					driveDTO.toString());
			throw new ResourceInvalidException("Candidate status change not allowed.");
		}
		DriveQO driveQO = conversionService.convert(driveDTO, DriveQO.class);
		updateDriveQO(driveQO);
		return getDriveByID(driveDTO.getId());
	}

	public List<DriveDTO> getMyDrives() {
		List<DriveQO> driveQOList = driveDAO.getDrivesByCreator(authorityEvaluator.getCurrentUsername());
		List<DriveDTO> driveDTOList = new ArrayList<>();
		if (driveQOList != null && driveQOList.size() > 0)
			driveDTOList = conversionService.convert(driveQOList, DriveDTO.class);
		return driveDTOList;
	}

	public List<DriveDTO> getAllDrives() {
		List<DriveQO> driveQOList = driveDAO.getAllDrives(authorityEvaluator.getCurrentUsername());
		List<DriveDTO> driveDTOList = new ArrayList<>();
		if (driveQOList != null && driveQOList.size() > 0)
			driveDTOList = conversionService.convert(driveQOList, DriveDTO.class);
		return driveDTOList;
	}

	public List<DriveDTO> getMyTeamsDrives() {
		List<DriveQO> driveQOList = driveDAO.getMyTeamsDrives(authorityEvaluator.getCurrentUsername());
		List<DriveDTO> driveDTOList = new ArrayList<>();
		if (driveQOList != null && driveQOList.size() > 0)
			driveDTOList = conversionService.convert(driveQOList, DriveDTO.class);
		return driveDTOList;
	}
}
