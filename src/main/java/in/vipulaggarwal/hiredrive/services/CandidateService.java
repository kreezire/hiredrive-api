package in.vipulaggarwal.hiredrive.services;

import in.vipulaggarwal.hiredrive.db.qo.CandidateQO;
import in.vipulaggarwal.hiredrive.dto.CandidateDTO;
import in.vipulaggarwal.hiredrive.enums.CandidateStatus;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import java.io.InputStream;
import java.util.List;

public interface CandidateService {

	CandidateDTO addCandidate(InputStream uploadedInputStream, FormDataContentDisposition fileDetails,
			CandidateQO candidateQO);

	CandidateDTO getCandidateById(Integer id);

	void updateCandidateStatus(Integer id, CandidateStatus candidateStatus);

	CandidateDTO updateCandidate(InputStream uploadedInputStream, FormDataContentDisposition fileDetails,
			CandidateDTO candidateDTO);

	List<CandidateDTO> getCandidateListByDriveId(Integer driveId);

	List<CandidateDTO> getMyCandidates();
}
