package in.vipulaggarwal.hiredrive.services;

import java.io.InputStream;

public interface FileStorageService {
    void upload(InputStream inputStream, long fileSize, String path);

    String getStorageURIForContainer();
}

