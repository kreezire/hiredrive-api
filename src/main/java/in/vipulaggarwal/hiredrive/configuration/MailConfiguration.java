package in.vipulaggarwal.hiredrive.configuration;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import in.vipulaggarwal.hiredrive.services.EmailService;
import in.vipulaggarwal.hiredrive.services.impl.EmailServiceImpl;

@Configuration
public class MailConfiguration {
		
	@Value("${hiredrive.mail.host}")
	private String host;
	@Value("${hiredrive.mail.username}")
	private String username;
	@Value("${hiredrive.mail.password}")
	private String password;
	@Value("${hiredrive.mail.transport.protocol}")
	private String transportProtocol;
	@Value("#{new Integer('${hiredrive.mail.smtp.port}')}")
	private Integer port;
	@Value("#{new Boolean('${hiredrive.mail.smtp.auth}')}")
	private Boolean smtpAuth;
	@Value("#{new Boolean('${hiredrive.mail.smtp.starttls.enable}')}")
	private Boolean smtpStarttlsEnable;
	@Value("#{new Boolean('${hiredrive.mail.smtp.starttls.required}')}")
	private Boolean smtpStarttlsRequired;
	

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(port);
        
        mailSender.setUsername(username);
        mailSender.setPassword(password);
        
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", transportProtocol);
        props.put("mail.smtp.auth", smtpAuth);
        props.put("mail.smtp.starttls.enable", smtpStarttlsEnable);
        
        return mailSender;
    }
    
    @Bean
    public EmailService getEmailService() {
    	return new EmailServiceImpl();
    }
}
