package in.vipulaggarwal.hiredrive.configuration;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import in.vipulaggarwal.hiredrive.aspect.validator.BasicUserAuthValidator;
import in.vipulaggarwal.hiredrive.aspect.validator.DriveValidator;
import in.vipulaggarwal.hiredrive.common.ConversionService;
import in.vipulaggarwal.hiredrive.exceptions.exceptionMappers.ExceptionErrorMessageBuilder;
import in.vipulaggarwal.hiredrive.services.AnalyticsService;
import in.vipulaggarwal.hiredrive.services.CandidateService;
import in.vipulaggarwal.hiredrive.services.DriveService;
import in.vipulaggarwal.hiredrive.services.FileStorageService;
import in.vipulaggarwal.hiredrive.services.InterviewService;
import in.vipulaggarwal.hiredrive.services.InterviewerService;
import in.vipulaggarwal.hiredrive.services.ReportGenerationService;
import in.vipulaggarwal.hiredrive.services.ResumeParser;
import in.vipulaggarwal.hiredrive.services.RoundService;
import in.vipulaggarwal.hiredrive.services.UserService;
import in.vipulaggarwal.hiredrive.services.impl.AnalyticsServiceImpl;
import in.vipulaggarwal.hiredrive.services.impl.AzureFileStorageService;
import in.vipulaggarwal.hiredrive.services.impl.CandidateReportGeneration;
import in.vipulaggarwal.hiredrive.services.impl.CandidateServiceImpl;
import in.vipulaggarwal.hiredrive.services.impl.DriveServiceImpl;
import in.vipulaggarwal.hiredrive.services.impl.InterviewServiceImpl;
import in.vipulaggarwal.hiredrive.services.impl.InterviewerServiceImpl;
import in.vipulaggarwal.hiredrive.services.impl.ProxyService;
import in.vipulaggarwal.hiredrive.services.impl.ResumeParserImpl;
import in.vipulaggarwal.hiredrive.services.impl.RoundServiceImpl;
import in.vipulaggarwal.hiredrive.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;

@Configuration
public class ServicesConfiguration {

	@Value("${azure.storage.connection-string}")
	private String azureStorageConnectionString;

	@Value("${hiredrive.resume.storage.container}")
	private String resumeStorageContainer;

	@Bean
	public ConversionService getConversionService() {
		return new ConversionService();
	}

	@Bean
	public UserService getUserService() {
		return new UserServiceImpl();
	}

	@Bean
	public DriveService getDriveService() {
		return new DriveServiceImpl();
	}

	@Bean
	public BasicUserAuthValidator getBasicUserAuthValidator() {
		return new BasicUserAuthValidator();
	}

	@Bean
	public DriveValidator getDriveValidator() {
		return new DriveValidator();
	}

	@Bean
	public CandidateService getCandidateService() {
		return new CandidateServiceImpl();
	}

	@Bean
	public RoundService getRoundService() {
		return new RoundServiceImpl();
	}

	@Bean
	public ReportGenerationService getReportGenerationService() {
		return new CandidateReportGeneration();
	}

	@Bean
	public ExceptionErrorMessageBuilder getExceptionErrorMessageBuilder() {
		return new ExceptionErrorMessageBuilder();
	}

	@Bean
	public CloudBlobClient getCloudBlobClient() throws URISyntaxException, InvalidKeyException {
		CloudStorageAccount storageAccount = CloudStorageAccount.parse(azureStorageConnectionString);
		return storageAccount.createCloudBlobClient();
	}

	@Bean
	public FileStorageService getAzureFileStorageService() {
		return new AzureFileStorageService(resumeStorageContainer);
	}

	@Bean
	public ResumeParser getResumeParser() {
		return new ResumeParserImpl();
	}

	@Bean
	public ProxyService getProxyService() {
		return new ProxyService();
	}

	@Bean
	public InterviewService getFeedbackService() {
		return new InterviewServiceImpl();
	}

	@Bean
	public InterviewerService getInterviewerService() {
		return new InterviewerServiceImpl();
	}

	@Bean
	public AnalyticsService getAnalyticsService() {
		return new AnalyticsServiceImpl();
	}
}
