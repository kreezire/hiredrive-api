package in.vipulaggarwal.hiredrive.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration {
	
	 @Value("${hiredrive.cors.allowed}")
	 private String[] allowedCors;
	 
	 @Value("${hiredrive.cors.allowed.methods}")
	 private String[] allowedMethods;
	 
	@Bean
    public WebMvcConfigurer corsConfigurer() {
      return new WebMvcConfigurer() {
        @Override
        public void addCorsMappings(CorsRegistry registry) {
          registry.addMapping("/**").allowedOrigins(allowedCors).allowedMethods(allowedMethods);
        }
      };
    }

}
