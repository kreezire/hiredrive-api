package in.vipulaggarwal.hiredrive.configuration;

import in.vipulaggarwal.hiredrive.db.dao.BasicUserAuthDAO;
import in.vipulaggarwal.hiredrive.db.dao.BasicUserDAO;
import in.vipulaggarwal.hiredrive.db.dao.DriveDAO;
import in.vipulaggarwal.hiredrive.db.dao.InterviewDAO;
import in.vipulaggarwal.hiredrive.db.dao.InterviewerDAO;
import in.vipulaggarwal.hiredrive.db.dao.PasswordResetDAO;
import in.vipulaggarwal.hiredrive.db.dao.RoundDAO;
import in.vipulaggarwal.hiredrive.db.dao.CandidateDAO;
import in.vipulaggarwal.hiredrive.db.dao.impl.BasicUserAuthDAOImpl;
import in.vipulaggarwal.hiredrive.db.dao.impl.BasicUserDAOImpl;
import in.vipulaggarwal.hiredrive.db.dao.impl.DriveDAOImpl;
import in.vipulaggarwal.hiredrive.db.dao.impl.InterviewDAOImpl;
import in.vipulaggarwal.hiredrive.db.dao.impl.InterviewerDAOImpl;
import in.vipulaggarwal.hiredrive.db.dao.impl.PasswordResetDAOImpl;
import in.vipulaggarwal.hiredrive.db.dao.impl.RoundDAOImpl;
import in.vipulaggarwal.hiredrive.db.dao.impl.CandidateDAOImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DAOConfiguration {
	@Bean
	public BasicUserAuthDAO getBasicUserAuthDAO() {
		return new BasicUserAuthDAOImpl();
	}

	@Bean
	public BasicUserDAO getBasicUserDAO() {
		return new BasicUserDAOImpl();
	}

	@Bean
	public DriveDAO getDriveDAO() {
		return new DriveDAOImpl();
	}

	@Bean
	public CandidateDAO getCandidateDAO() {
		return new CandidateDAOImpl();
	}

	@Bean
	public RoundDAO getRoundDAO() {
		return new RoundDAOImpl();
	}

	@Bean
	public InterviewDAO getInterviewDAO() {
		return new InterviewDAOImpl();
	}

	@Bean
	public InterviewerDAO getInterviewerDAO() {
		return new InterviewerDAOImpl();
	}

	@Bean
	public PasswordResetDAO getPasswordResetDAO() {
		return new PasswordResetDAOImpl();
	}
}
