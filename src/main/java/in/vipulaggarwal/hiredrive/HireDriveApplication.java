package in.vipulaggarwal.hiredrive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@ComponentScan({"in.vipulaggarwal"})
@EnableAsync
public class HireDriveApplication extends AsyncConfigurerSupport {

	public static void main(String[] args) {
		SpringApplication.run(HireDriveApplication.class, args);
	}

}
