package in.vipulaggarwal.hiredrive.dto;

import in.vipulaggarwal.hiredrive.aspect.ValidInterviewer;

@ValidInterviewer
public class InterviewerDTO {
	private Integer id;
	private String emailId;
	private Integer driveId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Integer getDriveId() {
		return driveId;
	}

	public void setDriveId(Integer driveId) {
		this.driveId = driveId;
	}
}
