package in.vipulaggarwal.hiredrive.dto;

public interface BasicUserDTO {

    String getFirstName();

    void setFirstName(String firstName);

    String getLastName();

    void setLastName(String lastName);

    String getEmailId();

   void setEmailId(String emailId);
}
