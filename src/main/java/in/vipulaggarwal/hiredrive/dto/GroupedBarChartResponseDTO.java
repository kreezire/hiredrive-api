package in.vipulaggarwal.hiredrive.dto;

import java.util.List;

public class GroupedBarChartResponseDTO<T1, T2>  {
	
	List<T2> subGroup;
	List<T1> xData;
	List<List<Integer>> yData;

	
	public List<T1> getxData() {
		return xData;
	}

	public void setxData(List<T1> xData) {
		this.xData = xData;
	}

	public List<T2> getSubGroup() {
		return subGroup;
	}

	public void setSubGroup(List<T2> subGroup) {
		this.subGroup = subGroup;
	}


	public List<List<Integer>> getyData() {
		return yData;
	}

	public void setyData(List<List<Integer>> yData) {
		this.yData = yData;
	}
	
	
	

}
