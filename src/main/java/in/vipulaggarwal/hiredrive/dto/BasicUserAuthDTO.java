package in.vipulaggarwal.hiredrive.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import in.vipulaggarwal.hiredrive.aspect.ValidBasicUserAuth;
import in.vipulaggarwal.hiredrive.dto.impl.BasicUserAuthDTOImpl;


@JsonDeserialize(as = BasicUserAuthDTOImpl.class)
@ValidBasicUserAuth
public interface BasicUserAuthDTO extends BasicUserDTO {

    String getPassword();
    //TODO add confirm password
    void setPassword(String password);
}
