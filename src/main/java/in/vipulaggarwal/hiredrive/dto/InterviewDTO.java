package in.vipulaggarwal.hiredrive.dto;

import in.vipulaggarwal.hiredrive.aspect.ValidInterview;
import in.vipulaggarwal.hiredrive.enums.InterviewStatus;

import java.sql.Timestamp;

@ValidInterview
public class InterviewDTO {
	private Integer id;
	private Long startTime;
	private Integer rating;
	private Long submitTime;
	private String interviewer;
	private String feedback;
	private InterviewStatus status;
	private Integer candidateId;
	private Integer roundId;
	private String feedbackDraft;
	private Integer duration; // in minutes

	public String getFeedbackDraft() {
		return feedbackDraft;
	}

	public void setFeedbackDraft(String feedbackDraft) {
		this.feedbackDraft = feedbackDraft;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Long submitTime) {
		this.submitTime = submitTime;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public String getInterviewer() {
		return interviewer;
	}

	public void setInterviewer(String interviewer) {
		this.interviewer = interviewer;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public InterviewStatus getStatus() {
		return status;
	}

	public void setStatus(InterviewStatus status) {
		this.status = status;
	}

	public Integer getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(Integer candidateId) {
		this.candidateId = candidateId;
	}

	public Integer getRoundId() {
		return roundId;
	}

	public void setRoundId(Integer roundId) {
		this.roundId = roundId;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}
}
