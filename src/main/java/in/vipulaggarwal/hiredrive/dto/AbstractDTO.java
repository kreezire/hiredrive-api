package in.vipulaggarwal.hiredrive.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Timestamp;

public class AbstractDTO {
    private String createdUserId;
    private String modifiedUserId;
    private Timestamp createdTime;
    private Timestamp modifiedTime;

    public AbstractDTO() {

    }

    public AbstractDTO(AbstractDTO other) {
        this.createdUserId = other.createdUserId;
        this.modifiedUserId = other.modifiedUserId;
        this.createdTime = other.createdTime;
        this.modifiedTime = other.modifiedTime;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public String getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(String modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public Timestamp getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Timestamp modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    @JsonIgnore
    public Long getCreatedTimeLong() {
        return createdTime != null ? createdTime.getTime() : null;
    }

    @JsonIgnore
    public Long getModifiedTimeLong() {
        return modifiedTime != null ? modifiedTime.getTime() : null;
    }
}


