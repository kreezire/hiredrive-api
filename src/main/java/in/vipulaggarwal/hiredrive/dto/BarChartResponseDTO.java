package in.vipulaggarwal.hiredrive.dto;

import java.util.List;

public class BarChartResponseDTO<T> {
	private List<T> xData;
	private List<Integer> yData;
	
	public List<T> getxData() {
		return xData;
	}
	public void setxData(List<T> xData) {
		this.xData = xData;
	}
	public List<Integer> getyData() {
		return yData;
	}
	public void setyData(List<Integer> yData) {
		this.yData = yData;
	}
}
