package in.vipulaggarwal.hiredrive.dto.impl;

import in.vipulaggarwal.hiredrive.dto.BasicUserDTO;

public class BasicUserDTOImpl implements BasicUserDTO {
    private String firstName;
    private String lastName;
    private String emailId;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
