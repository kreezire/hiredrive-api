package in.vipulaggarwal.hiredrive.dto.impl;

import in.vipulaggarwal.hiredrive.dto.BasicUserAuthDTO;

public class BasicUserAuthDTOImpl extends BasicUserDTOImpl implements BasicUserAuthDTO {

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
