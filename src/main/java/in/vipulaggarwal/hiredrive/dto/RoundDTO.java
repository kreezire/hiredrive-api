package in.vipulaggarwal.hiredrive.dto;

import in.vipulaggarwal.hiredrive.aspect.ValidRound;
import in.vipulaggarwal.hiredrive.enums.EntityType;

@ValidRound
public class RoundDTO {
	private Integer id;
	private String name;
	private Integer entityId; // can be candidateId or driveId
	private EntityType entityType; // can be "CANDIDATE" or "DRIVE"
	private boolean required;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

}
