package in.vipulaggarwal.hiredrive.dto;

import in.vipulaggarwal.hiredrive.aspect.ValidDrive;
import in.vipulaggarwal.hiredrive.enums.DriveStatus;

@ValidDrive
public class DriveDTO {
	private Integer id;
	private String name;
	private DriveStatus status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DriveStatus getStatus() {
		return status;
	}

	public void setStatus(DriveStatus status) {
		this.status = status;
	}

}
