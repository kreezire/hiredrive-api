package in.vipulaggarwal.hiredrive.auth.config;

import in.vipulaggarwal.hiredrive.auth.filter.JwtRequestFilter;
import in.vipulaggarwal.hiredrive.auth.services.AuthorityEvaluator;
import in.vipulaggarwal.hiredrive.auth.services.JwtUtilService;
import in.vipulaggarwal.hiredrive.auth.services.impl.AuthorityEvaluatorImpl;
import in.vipulaggarwal.hiredrive.auth.services.impl.RESTAuthenticationEntryPoint;
import in.vipulaggarwal.hiredrive.auth.services.impl.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
public class AuthConfig {
    @Bean
    public PasswordEncoder getPasswordEncoder () {
        return new BCryptPasswordEncoder();
    }

    @Bean(name = "userDetailsServiceImpl")
    public UserDetailsService getUserDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Bean(name = "jwtAccessTokenConverterDefault")
    JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        return converter;
    }

    @Bean
    public JwtUtilService getJwtUtilService() {
        return new JwtUtilService();
    }

    @Bean
    public AuthorityEvaluator getAuthorityEvaluator() {
        return new AuthorityEvaluatorImpl();
    }

    @Bean
    public RESTAuthenticationEntryPoint getRESTAuthenticationEntryPoint() {
        return new RESTAuthenticationEntryPoint();
    }

    @Bean
    public JwtRequestFilter getJwtRequestFilter() {
        return new JwtRequestFilter();
    }

}
