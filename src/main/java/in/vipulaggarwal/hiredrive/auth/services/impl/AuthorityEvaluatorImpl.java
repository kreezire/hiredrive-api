package in.vipulaggarwal.hiredrive.auth.services.impl;

import in.vipulaggarwal.hiredrive.auth.services.AuthorityEvaluator;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

public class AuthorityEvaluatorImpl implements AuthorityEvaluator {

	public String getCurrentUsername() {
		return getCurrentUser().getUsername();
	}

	private User getCurrentUser() {
		return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}
