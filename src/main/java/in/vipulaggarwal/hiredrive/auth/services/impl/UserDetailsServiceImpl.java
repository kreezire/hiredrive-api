package in.vipulaggarwal.hiredrive.auth.services.impl;

import in.vipulaggarwal.hiredrive.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserDetails userDetails = userService.getUserDetailsByUsername(username);

        if(userDetails == null) {
            throw new UsernameNotFoundException("User not found.");
        }

        return userDetails;
    }
}
