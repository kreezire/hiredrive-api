package in.vipulaggarwal.hiredrive.auth.services;

public interface AuthorityEvaluator {

    String getCurrentUsername();
}
