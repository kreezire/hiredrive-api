package in.vipulaggarwal.hiredrive.enums;

public enum CandidateStatus {

    NEW,
    IN_PROGRESS,
    WAITING,
    REJECTED,
    SELECTED;

    private CandidateStatus() {

    }

    public static CandidateStatus fromString(String value) {
        try {
            return valueOf(value);
        } catch (Exception var2) {
            return null;
        }
    }
}
