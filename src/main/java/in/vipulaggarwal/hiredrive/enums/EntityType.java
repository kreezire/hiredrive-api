package in.vipulaggarwal.hiredrive.enums;

public enum EntityType {

	DRIVE, CANDIDATE;

	private EntityType() {

	}

	public static EntityType fromString(String value) {
		try {
			return valueOf(value.toUpperCase());
		} catch (Exception var2) {
			return null;
		}
	}
}
