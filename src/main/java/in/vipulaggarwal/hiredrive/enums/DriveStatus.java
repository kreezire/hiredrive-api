package in.vipulaggarwal.hiredrive.enums;

public enum DriveStatus {

	OPEN, CLOSE;

	private DriveStatus() {

	}

	public static DriveStatus fromString(String value) {
		try {
			return valueOf(value);
		} catch (Exception var2) {
			return null;
		}
	}
}
