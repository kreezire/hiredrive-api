package in.vipulaggarwal.hiredrive.enums;

public enum Gender {
	MALE, FEMALE, UNSPECIFIED, OTHER;

	private Gender() {

	}

	public static Gender fromString(String value) {
		try {
			return valueOf(value.toUpperCase());
		} catch (Exception var2) {
			return null;
		}
	}
}
