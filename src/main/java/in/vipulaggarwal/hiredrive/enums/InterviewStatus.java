package in.vipulaggarwal.hiredrive.enums;

public enum InterviewStatus {
    PENDING,
    ONGOING,
    DONE;

    private InterviewStatus() {

    }

    public static InterviewStatus fromString(String value) {
        try {
            return valueOf(value.toUpperCase());
        } catch (Exception var2) {
            return null;
        }
    }
}
