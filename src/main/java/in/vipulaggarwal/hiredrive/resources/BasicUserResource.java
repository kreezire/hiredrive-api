package in.vipulaggarwal.hiredrive.resources;

import in.vipulaggarwal.hiredrive.auth.entity.AuthenticationResponse;
import in.vipulaggarwal.hiredrive.auth.services.AuthorityEvaluator;
import in.vipulaggarwal.hiredrive.auth.services.JwtUtilService;
import in.vipulaggarwal.hiredrive.dto.BasicUserAuthDTO;
import in.vipulaggarwal.hiredrive.dto.BasicUserDTO;
import in.vipulaggarwal.hiredrive.dto.PasswordDTO;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.exceptions.UnauthorizedException;
import in.vipulaggarwal.hiredrive.services.UserService;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class BasicUserResource {

	@Autowired
	private UserService userService;

	@Autowired
	private UserDetailsService userDetailsServiceImpl;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtUtilService jwtUtilService;

	@Autowired
	AuthorityEvaluator authorityEvaluator;

	public BasicUserResource() {

	}

	@POST
	@Path("/register")
	public BasicUserDTO registerUser(@NotNull @Valid BasicUserAuthDTO basicUserAuthDTO) {
		if (StringUtils.isEmpty(basicUserAuthDTO.getPassword())) {
			throw new ResourceInvalidException("Invalid Password!");
		}
		return userService.add(basicUserAuthDTO);
	}

	@POST
	@Path("/login")
	public AuthenticationResponse createAuthenticationToken(@NotNull BasicUserAuthDTO basicUserAuthDTO) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(basicUserAuthDTO.getEmailId(),
					basicUserAuthDTO.getPassword()));
		} catch (BadCredentialsException e) {
			// TODO check if we need custom exception
			throw new UnauthorizedException("Bad Credentials");
		}
		final UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(basicUserAuthDTO.getEmailId());
		final String jwt = jwtUtilService.generateToken(userDetails);

		return new AuthenticationResponse(jwt);
	}

	@GET
	@Path("/me")
	public BasicUserDTO getMyProfile() {
		String username = authorityEvaluator.getCurrentUsername();
		if (StringUtils.isEmpty(username)) {
			throw new ResourceInvalidException("Invalid User.");
		}

		return userService.getUserByUsername(username);
	}

	@GET
	@Path("/email")
	public BasicUserDTO getProfileByEmailId(@NotNull @QueryParam("emailId") String emailId) {
		if (StringUtils.isEmpty(emailId)) {
			throw new ResourceInvalidException("Invalid User.");
		}
		return userService.getUserByUsername(emailId);
	}

	@PUT
	@Path("/password")
	public BasicUserDTO changePassword(@NotNull String passData) {
		String currentPassword = null;
		String newPassword = null;
		try {
			JSONObject json = new JSONObject(passData);
			currentPassword = json.getString("currentPassword");
			newPassword = json.getString("newPassword");
		} catch (JSONException e) {
			throw new ResourceInvalidException("Invalid Request!");
		}
		if (StringUtils.isEmpty(currentPassword)) {
			throw new ResourceInvalidException("Incorrect current Password!");
		}
		if (StringUtils.isEmpty(newPassword)) {
			throw new ResourceInvalidException("Incorrect new Password!");
		}
		BasicUserDTO basicUserDTO = getMyProfile();
		return userService.changePassword(basicUserDTO, currentPassword, newPassword);
	}

	@POST
	@Path("/forgot-password")
	public BasicUserDTO forgotPassword(@NotNull PasswordDTO passwordDTO) {
		if (StringUtils.isEmpty(passwordDTO.getEmailId())) {
			throw new ResourceInvalidException("Incorrect EmailID!");
		}
		return userService.forgotPassword(passwordDTO.getEmailId());
	}

	@POST
	@Path("/reset-password")
	public void resetPassword(@NotNull PasswordDTO passwordDTO) {
		if (StringUtils.isEmpty(passwordDTO.getEmailId())) {
			throw new ResourceInvalidException("Incorrect EmailID!");
		}
		if (StringUtils.isEmpty(passwordDTO.getToken())) {
			throw new ResourceInvalidException("Incorrect Token!");
		}
		if (StringUtils.isEmpty(passwordDTO.getNewPassword())) {
			throw new ResourceInvalidException("Incorrect new Password!");
		}
		if (StringUtils.isEmpty(passwordDTO.getConfirmPassword())) {
			throw new ResourceInvalidException("Incorrect confirm Password!");
		}
		if (!passwordDTO.getNewPassword().equals(passwordDTO.getConfirmPassword())) {
			throw new ResourceInvalidException("Confirm Password is Incorrect!");
		}
		userService.resetPassword(passwordDTO.getEmailId(), passwordDTO.getToken(), passwordDTO.getNewPassword(),
				passwordDTO.getConfirmPassword());
	}

}
