package in.vipulaggarwal.hiredrive.resources;

import in.vipulaggarwal.hiredrive.dto.RoundDTO;
import in.vipulaggarwal.hiredrive.enums.EntityType;
import in.vipulaggarwal.hiredrive.exceptions.ResourceAddException;
import in.vipulaggarwal.hiredrive.services.RoundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/round")
@Produces(MediaType.APPLICATION_JSON)
public class RoundResource {
	private static final Logger logger = LoggerFactory.getLogger(RoundResource.class);

	@Autowired
	RoundService roundService;

	@POST
	public RoundDTO addRoundInfo(@NotNull @Valid RoundDTO roundDTO) {
		RoundDTO response = null;
		try {
			response = roundService.addRoundInfo(roundDTO, roundDTO.getEntityId(), roundDTO.getEntityType());
		} catch (Exception e) {
			logger.error("Unable to add Round.");
			throw new ResourceAddException("Unable to add Round.");
		}
		return response;
	}

	@GET
	@Path("/{entityParam}/{id}")
	public List<RoundDTO> getRoundByEntityIdAndEntityType(@NotNull @PathParam("entityParam") EntityType entityParam,
			@NotNull @PathParam("id") Integer id) {
		return roundService.getRoundByEntityIdAndEntityType(id, entityParam);
	}

	@GET
	@Path("/{id}")
	public RoundDTO getRoundByID(@NotNull @PathParam("id") Integer id) {
		return roundService.getRoundByID(id);
	}

	@PUT
	public RoundDTO updateRoundByID(@NotNull @Valid RoundDTO newRoundDTO) {
		return roundService.updateRoundByID(newRoundDTO);
	}

	@DELETE
	@Path("/{id}")
	public void deleteRound(@NotNull @PathParam("id") Integer id) {
		roundService.delete(id);
	}

}
