package in.vipulaggarwal.hiredrive.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import in.vipulaggarwal.hiredrive.services.ReportGenerationService;

@Path("/report")
@Produces(MediaType.APPLICATION_OCTET_STREAM)
public class ReportGenerationResource {

	private static final Logger logger = LoggerFactory.getLogger(ReportGenerationResource.class);

	@Autowired
	ReportGenerationService reportGenerationService;

	@GET
	@Path("/candidate/{id}")
	public Response getCandidateReport(@NotNull @PathParam("id") Integer id) throws IOException {
		reportGenerationService.genReport(id);
		String fileLocation = "sample.pdf";
		Response response = null;
		NumberFormat myFormat = NumberFormat.getInstance();
		myFormat.setGroupingUsed(true);

		File file = new File(fileLocation);

		InputStream inputStream = new FileInputStream(file);
		byte[] bytes = new byte[(int) file.length()];
		inputStream.read(bytes);

		if (file.exists()) {
			ResponseBuilder builder = Response.ok(bytes);
			builder.header("Content-Disposition", "attachment; filename=" + file.getName());
			response = builder.build();
			long file_size = file.length();
			logger.info(String.format("Inside getCandidateReport==> fileName: %s, fileSize: %s bytes", fileLocation,
					myFormat.format(file_size)));
		} else {
			logger.error(String.format("Inside getCandidateReport==> FILE NOT FOUND: fileName: %s", fileLocation));
			response = Response.status(404).entity("FILE NOT FOUND: " + fileLocation).type("text/plain").build();
		}

		return response;
	}
}
