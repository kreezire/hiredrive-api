package in.vipulaggarwal.hiredrive.resources;

import in.vipulaggarwal.hiredrive.dto.InterviewerDTO;
import in.vipulaggarwal.hiredrive.exceptions.ResourceAddException;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.services.InterviewerService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/interviewer")
@Produces(MediaType.APPLICATION_JSON)
public class InterviewerResource {
	private static final Logger logger = LoggerFactory.getLogger(InterviewerResource.class);

	@Autowired
	InterviewerService interviewerService;

	@POST
	public InterviewerDTO addInterviewer(@NotNull @Valid InterviewerDTO interviewerDTO) {
		if (interviewerDTO.getId() != null) {
			logger.error("invalid interviewerDTO={} contains id", interviewerDTO);
			throw new ResourceAddException("Interviewer Object contains invalid ID. ");
		}

		if (interviewerDTO.getDriveId() == null || StringUtils.isEmpty(interviewerDTO.getEmailId())) {
			logger.error("invalid interviewerDTO={} for add.", interviewerDTO);
			throw new ResourceAddException("Invalid Resource.");
		}

		return interviewerService.addInterviewer(interviewerDTO);
	}

	@GET
	@Path("/{driveId}")
	public List<InterviewerDTO> getInterviewersByDriveId(@NotNull @PathParam("driveId") Integer driveId) {
		return interviewerService.getInterviewersByDriveId(driveId);
	}

	@PUT
	public InterviewerDTO updateInterviewer(@NotNull @Valid InterviewerDTO interviewerDTO) {
		if (interviewerDTO.getId() == null) {
			logger.error("invalid null id for interviewer={} update.", interviewerDTO);
			throw new ResourceInvalidException("Invalid update for interviewer.");
		}

		return interviewerService.update(interviewerDTO);
	}

	@DELETE
	@Path("/{id}")
	public void deleteInterviewer(@NotNull @PathParam("id") Integer id) {
		interviewerService.delete(id);
	}
}
