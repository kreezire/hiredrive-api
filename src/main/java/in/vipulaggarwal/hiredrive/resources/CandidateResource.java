package in.vipulaggarwal.hiredrive.resources;

import in.vipulaggarwal.hiredrive.common.utils.ObjUtils;
import in.vipulaggarwal.hiredrive.db.qo.CandidateQO;
import in.vipulaggarwal.hiredrive.dto.CandidateDTO;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.services.CandidateService;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;
import java.util.List;

@Path("/candidate")
@Produces(MediaType.APPLICATION_JSON)
public class CandidateResource {
	private static final Logger logger = LoggerFactory.getLogger(CandidateResource.class);

	@Autowired
	CandidateService candidateService;

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public CandidateDTO addCandidate(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetails,
			@FormDataParam("candidate") FormDataBodyPart candidateJSONpart) {

		CandidateQO candidateQO = null;
		try {
			candidateJSONpart.setMediaType(MediaType.APPLICATION_JSON_TYPE);
			candidateQO = candidateJSONpart.getValueAs(CandidateQO.class);
		} catch (Exception e) {
			logger.debug("Unable to make CandidateQO from POST data.", e);
		}

		if (!ObjUtils.validFile(fileDetails) && !ObjUtils.validForAddCandidateQO(candidateQO)) {
			logger.error("Invalid Candidate POST.");
			throw new ResourceInvalidException("Invalid add Candidate request.");
		}

		return candidateService.addCandidate(uploadedInputStream, fileDetails, candidateQO);
	}

	@GET
	@Path("/{id}")
	public CandidateDTO getCandidateById(@NotNull @PathParam("id") Integer id) {
		return candidateService.getCandidateById(id);
	}

	@PUT
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public CandidateDTO updateCandidate(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetails,
			@FormDataParam("candidate") FormDataBodyPart candidateJSONpart) {
		CandidateDTO candidateDTO = null;
		try {
			candidateJSONpart.setMediaType(MediaType.APPLICATION_JSON_TYPE);
			candidateDTO = candidateJSONpart.getValueAs(CandidateDTO.class);
		} catch (Exception e) {
			logger.debug("Unable to make CandidateDTO from PUT data.", e);
		}

		if (candidateDTO == null || candidateDTO.getId() == null) {
			logger.error("candidate id is null.");
			throw new ResourceInvalidException("Invalid candidate.");
		}

		return candidateService.updateCandidate(uploadedInputStream, fileDetails, candidateDTO);
	}

	@GET
	@Path("/me")
	public List<CandidateDTO> getMyCandidates() {

		return candidateService.getMyCandidates();

	}

}
