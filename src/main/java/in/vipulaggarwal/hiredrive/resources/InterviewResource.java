package in.vipulaggarwal.hiredrive.resources;

import in.vipulaggarwal.hiredrive.dto.InterviewCandidateRoundDTO;
import in.vipulaggarwal.hiredrive.dto.InterviewDTO;
import in.vipulaggarwal.hiredrive.exceptions.ResourceAddException;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.services.InterviewService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/interview")
@Produces(MediaType.APPLICATION_JSON)
public class InterviewResource {
	private static final Logger logger = LoggerFactory.getLogger(InterviewResource.class);

	@Autowired
	InterviewService interviewService;

	@POST
	public InterviewDTO addInterview(@NotNull @Valid InterviewDTO interviewDTO) {

		if (interviewDTO.getId() != null) {
			logger.error("invalid interviewDTO={} contains id", interviewDTO);
			throw new ResourceAddException("Interview Object contains invalid ID. ");
		}
		return interviewService.addInterview(interviewDTO);
	}

	@PUT
	public InterviewDTO updateInterview(@NotNull @Valid InterviewDTO interviewDTO) {
		if (interviewDTO.getId() == null) {
			logger.error("invalid null id for interview={} update.", interviewDTO);
			throw new ResourceInvalidException("Invalid update for interview.");
		}

		return interviewService.update(interviewDTO);
	}

	@GET
	@Path("/{candidateId}/{roundId}")
	public InterviewDTO getInterviewByCandidateIdAndRoundId(@NotNull @PathParam("candidateId") Integer candidateId,
			@NotNull @PathParam("roundId") Integer roundId) {
		return interviewService.getByCandidateIdAndRoundId(candidateId, roundId);
	}

	@GET
	@Path("/{candidateId}")
	public List<InterviewDTO> getInterviewByCandidateId(@NotNull @PathParam("candidateId") Integer candidateId) {
		return interviewService.getInterviewByCandidateId(candidateId);
	}

	@GET
	public List<InterviewDTO> getInterviewAssignedToMe() {
		return interviewService.getInterviewAssignedToMe();
	}
	
	@GET
	@Path("/me")
	public List<InterviewCandidateRoundDTO> getInterviewCandidateRoundDetailsAssignedToMe() {
		return interviewService.getInterviewCandidateRoundDetailsAssignedToMe();
	}
}
