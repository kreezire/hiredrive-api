package in.vipulaggarwal.hiredrive.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/health")
@Produces(MediaType.APPLICATION_JSON)
public class HealthResource {

	@GET
	@Path("/ping")
	public String healthPingPong() {
		return "pong";
	}
}
