package in.vipulaggarwal.hiredrive.resources;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;

import in.vipulaggarwal.hiredrive.dto.BarChartResponseDTO;
import in.vipulaggarwal.hiredrive.dto.GroupedBarChartResponseDTO;
import in.vipulaggarwal.hiredrive.services.AnalyticsService;

@Path("/analytics")
@Produces(MediaType.APPLICATION_JSON)
public class AnalyticsResource {

	@Autowired
	private AnalyticsService analyticsService;
	
	@GET
	@Path("/drive/{driveId}/stats/rounds")
	public BarChartResponseDTO<String> getRoundStatsForDrive (@NotNull @PathParam("driveId") Integer driveId) {
		return analyticsService.getRoundStatsForDrive(driveId);
	}
	
	@GET
	@Path("/drive/{driveId}/stats/interviewer")
	public BarChartResponseDTO<String> getInterviewerStatsForDrive (@NotNull @PathParam("driveId") Integer driveId) {
		return analyticsService.getInterviewerStatsForDrive(driveId);
	}
	
	@GET
	@Path("/drive/{driveId}/stats/talentpartner")
	public GroupedBarChartResponseDTO<String, String> getTalentPartnerStatsForDrive (@NotNull @PathParam("driveId") Integer driveId) {
		return analyticsService.getTalentPartnerStatsForDrive(driveId);
	}
	
	
}
