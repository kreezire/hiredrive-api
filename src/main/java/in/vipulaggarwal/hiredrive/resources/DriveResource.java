package in.vipulaggarwal.hiredrive.resources;

import in.vipulaggarwal.hiredrive.dto.CandidateDTO;
import in.vipulaggarwal.hiredrive.dto.DriveDTO;
import in.vipulaggarwal.hiredrive.enums.DriveStatus;
import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import in.vipulaggarwal.hiredrive.services.CandidateService;
import in.vipulaggarwal.hiredrive.services.DriveService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/drive")
@Produces(MediaType.APPLICATION_JSON)
public class DriveResource {
	private static final Logger logger = LoggerFactory.getLogger(DriveResource.class);

	@Autowired
	private DriveService driveService;

	@Autowired
	private CandidateService candidateService;

	public DriveResource() {

	}

	@POST
	public DriveDTO addDrive(@NotNull @Valid DriveDTO driveDTO) { // add new drive
		if (driveDTO.getId() != null) {
			logger.error("Invalid id in driveDTO={}", driveDTO);
			throw new ResourceInvalidException("Invalid id.");
		}

		if (driveDTO.getStatus() == null) {
			driveDTO.setStatus(DriveStatus.CLOSE);
		}
		return driveService.addDrive(driveDTO);
	}

	@GET
	@Path("/{id}")
	public DriveDTO getDriveById(@NotNull @PathParam("id") Integer id) { // get drive by id
		return driveService.getDriveByID(id);
	}

	@PUT
	public DriveDTO updateDrive(@NotNull @Valid DriveDTO driveDTO) {
		if (driveDTO.getId() == null) {
			logger.error("drive id is NULL.");
			throw new ResourceInvalidException("Invalid drive update.");
		}

		return driveService.updateDrive(driveDTO);
	}

	@GET
	@Path("/{id}/candidate")
	public List<CandidateDTO> getCandidateListByDriveId(@PathParam("id") Integer id) {
		return candidateService.getCandidateListByDriveId(id);
	}

	@GET
	@Path("/me")
	public List<DriveDTO> getMyDrives() {
		return driveService.getMyDrives();
	}

	@GET
	@Path("/team")
	// drives, created by my team and I'm part of them
	public List<DriveDTO> getMyTeamsDrives() {
		return driveService.getMyTeamsDrives();
	}

	@GET
	@Path("/all")
	// drives, created by me & my team and I'm part of them
	public List<DriveDTO> getAllDrives() {
		return driveService.getAllDrives();
	}

}
