package in.vipulaggarwal.hiredrive;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import in.vipulaggarwal.hiredrive.exceptions.exceptionMappers.AzureExceptionMapper;
import in.vipulaggarwal.hiredrive.exceptions.exceptionMappers.ExceptionErrorMessageBuilder;
import in.vipulaggarwal.hiredrive.exceptions.exceptionMappers.NotFoundExceptionMapper;
import in.vipulaggarwal.hiredrive.exceptions.exceptionMappers.ResourceAddExceptionMapper;
import in.vipulaggarwal.hiredrive.exceptions.exceptionMappers.ResourceInvalidExceptionMapper;
import in.vipulaggarwal.hiredrive.exceptions.exceptionMappers.UnauthorizedExceptionMapper;
import in.vipulaggarwal.hiredrive.resources.AnalyticsResource;
import in.vipulaggarwal.hiredrive.resources.BasicUserResource;
import in.vipulaggarwal.hiredrive.resources.CandidateResource;
import in.vipulaggarwal.hiredrive.resources.DriveResource;
import in.vipulaggarwal.hiredrive.resources.HealthResource;
import in.vipulaggarwal.hiredrive.resources.InterviewResource;
import in.vipulaggarwal.hiredrive.resources.InterviewerResource;
import in.vipulaggarwal.hiredrive.resources.ReportGenerationResource;
import in.vipulaggarwal.hiredrive.resources.RoundResource;

@Component
public class JerseyConfig extends ResourceConfig {
	public JerseyConfig() {
		// scan the resources package for our resources
		// packages("in.vipulaggarwal.hiredrive.resources",
		// "in.vipulaggarwal.hiredrive.exceptions.exceptionMappers");
		// Resources
		register(AnalyticsResource.class);
		register(BasicUserResource.class);
		register(CandidateResource.class);
		register(DriveResource.class);
		register(InterviewResource.class);
		register(InterviewerResource.class);
		register(ReportGenerationResource.class);
		register(RoundResource.class);
		register(HealthResource.class);

		// Exceptions
		register(AzureExceptionMapper.class);
		register(ExceptionErrorMessageBuilder.class);
		register(NotFoundExceptionMapper.class);
		register(ResourceAddExceptionMapper.class);
		register(ResourceInvalidExceptionMapper.class);
		register(UnauthorizedExceptionMapper.class);

		// Others
		register(MultiPartFeature.class);
	}
}
