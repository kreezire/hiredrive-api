package in.vipulaggarwal.hiredrive.db.qo;

public class InterviewerAnalyticsQO {

	private String interviewer;
	private Integer count;
	public String getInterviewer() {
		return interviewer;
	}
	public void setInterviewer(String interviewer) {
		this.interviewer = interviewer;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
	
}
