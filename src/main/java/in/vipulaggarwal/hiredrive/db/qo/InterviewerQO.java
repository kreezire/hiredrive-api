package in.vipulaggarwal.hiredrive.db.qo;

public class InterviewerQO extends AbstractQO {
	private Integer id;
	private String emailId;
	private Integer driveId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Integer getDriveId() {
		return driveId;
	}

	public void setDriveId(Integer driveId) {
		this.driveId = driveId;
	}
}
