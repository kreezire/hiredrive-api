package in.vipulaggarwal.hiredrive.db.qo;

public class RoundAnalyticsQO {

	private Integer roundId;
	private Integer count;
	public Integer getRoundId() {
		return roundId;
	}
	public void setRoundId(Integer roundId) {
		this.roundId = roundId;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
	
}
