package in.vipulaggarwal.hiredrive.db.qo;

public class RoundQO extends AbstractQO {
	private Integer id;
	private String name;
	private Integer entityId; // can be candidateId or driveId
	private String entityType; // can be "CANDIDATE" or "DRIVE"
	private boolean required;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
}
