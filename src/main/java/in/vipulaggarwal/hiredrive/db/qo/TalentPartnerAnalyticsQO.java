package in.vipulaggarwal.hiredrive.db.qo;

public class TalentPartnerAnalyticsQO {

	private Integer total;
	private Integer selected;
	private String talentPartner;
	
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getSelected() {
		return selected;
	}
	public void setSelected(Integer selected) {
		this.selected = selected;
	}
	public String getTalentPartner() {
		return talentPartner;
	}
	public void setTalentPartner(String talentPartner) {
		this.talentPartner = talentPartner;
	}
	
	
}
