package in.vipulaggarwal.hiredrive.db.qo;

public class CandidateQO extends AbstractQO {
	private Integer id;
	private String name;
	private String phone;
	private String emailId;
	private String resumePath;
	private Integer driveId;
	private String status;
	private String talentPartner;
	private Double yoe;
	private String skills;
	private String gender;
	
	

	public String getTalentPartner() {
		return talentPartner;
	}

	public void setTalentPartner(String talentPartner) {
		this.talentPartner = talentPartner;
	}

	public Double getYoe() {
		return yoe;
	}

	public void setYoe(Double yoe) {
		this.yoe = yoe;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getResumePath() {
		return resumePath;
	}

	public void setResumePath(String resumePath) {
		this.resumePath = resumePath;
	}

	public Integer getDriveId() {
		return driveId;
	}

	public void setDriveId(Integer driveId) {
		this.driveId = driveId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
