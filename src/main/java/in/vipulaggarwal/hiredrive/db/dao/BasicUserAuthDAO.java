package in.vipulaggarwal.hiredrive.db.dao;

import in.vipulaggarwal.hiredrive.db.qo.BasicUserAuthQO;

public interface BasicUserAuthDAO {
    Integer addUserAuth(BasicUserAuthQO basicUserAuthQO);

    BasicUserAuthQO getAuthDetailsByUserId(String userId);
    
    Integer updateUserAuth(BasicUserAuthQO basicUserAuthQO);
}
