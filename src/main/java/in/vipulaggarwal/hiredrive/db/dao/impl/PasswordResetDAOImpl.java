package in.vipulaggarwal.hiredrive.db.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import in.vipulaggarwal.hiredrive.db.dao.PasswordResetDAO;
import in.vipulaggarwal.hiredrive.db.qo.PasswordResetQO;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import in.vipulaggarwal.hiredrive.exceptions.ResourceAddException;

public class PasswordResetDAOImpl extends AbstractDAO implements PasswordResetDAO {
	private static final Logger logger = LoggerFactory.getLogger(PasswordResetDAOImpl.class);
	private static final String SQL_INSERT_STATEMENT = "INSERT INTO PASSWORD_RESET (EMAIL_ID, TOKEN)"
			+ "VALUES (:emailId, :token);";
	private static final String SQL_GET_BY_EMAIL_ID = "SELECT * FROM PASSWORD_RESET WHERE EMAIL_ID = :emailId;";
	private static final String SQL_DELETE_STATEMENT = "DELETE FROM PASSWORD_RESET WHERE EMAIL_ID=:emailId;";
	private static final String SQL_UPDATE_STATEMENT = "UPDATE PASSWORD_RESET SET TOKEN = :token"
			+ " WHERE EMAIL_ID = :emailId;";

	public Integer add(PasswordResetQO passwordResetQO) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("emailId", passwordResetQO.getEmailId());
			namedParameters.addValue("token", passwordResetQO.getToken());
			KeyHolder keyHolder = new GeneratedKeyHolder();
			int affectedRowCount = this.namedParameterJdbcTemplate.update(SQL_INSERT_STATEMENT, namedParameters,
					keyHolder);
			if (affectedRowCount == 1) {
				return keyHolder.getKey().intValue();
			} else {
				logger.error("PasswordReset details not added. PasswordResetQO={}", passwordResetQO);
				throw new ResourceAddException("Interviewer cannot be added to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while adding passwordResetQO={}", passwordResetQO, var5);
			throw new HDException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem adding passwordResetQO={} to DB", passwordResetQO, var6);
			throw new HDException(var6);
		}
	}

	public PasswordResetQO getByEmailId(String emailId) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("emailId", emailId);
			return this.namedParameterJdbcTemplate.queryForObject(SQL_GET_BY_EMAIL_ID, namedParameters,
					new BeanPropertyRowMapper<>(PasswordResetQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting PasswordResetQO info={} from DB", emailId, var4);
			throw new HDException(var4);

		}
	}

	public Integer update(PasswordResetQO passwordResetQO) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("emailId", passwordResetQO.getEmailId());
			namedParameters.addValue("token", passwordResetQO.getToken());
			int affectedRowCount = this.namedParameterJdbcTemplate.update(SQL_UPDATE_STATEMENT, namedParameters);
			if (affectedRowCount == 1) {
				return passwordResetQO.getId();
			} else {
				logger.error("Password reset details not updated. passwordResetQO={}", passwordResetQO);
				throw new ResourceAddException("Password reset details cannot be updated to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while updating passwordResetQO={}", passwordResetQO, var5);
			throw new ResourceAddException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem updating passwordResetQO={} to DB", passwordResetQO, var6);
			throw new ResourceAddException("Data access exception.");
		}
	}

	public boolean deleteByEmailId(String emailId) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("emailId", emailId);
			int affectedRowCount = namedParameterJdbcTemplate.update(SQL_DELETE_STATEMENT, namedParameters);
			return affectedRowCount == 1;
		} catch (DataAccessException e) {
			logger.error("Error while deleting PasswordReset info: ", e);
			throw new HDException(e);
		}
	}
}
