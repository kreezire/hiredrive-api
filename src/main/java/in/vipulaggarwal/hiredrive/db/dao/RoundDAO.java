package in.vipulaggarwal.hiredrive.db.dao;

import in.vipulaggarwal.hiredrive.db.qo.RoundQO;
import in.vipulaggarwal.hiredrive.enums.EntityType;

import java.util.List;

public interface RoundDAO {
	Integer addRound(RoundQO roundQO);

	RoundQO get(Integer id);

	List<RoundQO> getRoundByEntityIdAndEntityType(Integer entityId, EntityType entityType);

	Boolean addRounds(List<RoundQO> roundQOs, Integer entityId, String entityType);

	Integer update(RoundQO roundQO);

	boolean delete(Integer id);
}
