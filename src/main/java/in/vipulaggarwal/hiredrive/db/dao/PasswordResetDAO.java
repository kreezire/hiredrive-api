package in.vipulaggarwal.hiredrive.db.dao;

import in.vipulaggarwal.hiredrive.db.qo.PasswordResetQO;

public interface PasswordResetDAO {

	Integer add(PasswordResetQO passwordResetQO);

	PasswordResetQO getByEmailId(String emailId);

	boolean deleteByEmailId(String emailId);

	Integer update(PasswordResetQO passwordResetQO);
}
