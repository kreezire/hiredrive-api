package in.vipulaggarwal.hiredrive.db.dao;

import in.vipulaggarwal.hiredrive.db.qo.InterviewCandidateRoundQO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewQO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewerAnalyticsQO;
import in.vipulaggarwal.hiredrive.db.qo.RoundAnalyticsQO;

import java.util.List;
import java.util.Set;

public interface InterviewDAO {

	Integer add(InterviewQO interviewQO);

	InterviewQO get(Integer id);

	List<InterviewQO> getByCandidateIdAndStatus(Integer id, String status);

	Integer update(InterviewQO interviewQO);

	InterviewQO getByCandidateIdAndRoundId(Integer candidateId, Integer roundId);

	Integer getCountByRoundId(Integer roundId);

	List<InterviewQO> getInterviewByCandidateId(Integer candidateId);

	List<InterviewQO> getInterviewAssignedToMe(String username);

	List<InterviewCandidateRoundQO> getInterviewCandidateRoundDetailsAssignedToMe(String username);

	List<RoundAnalyticsQO> getRoundAnalytics(Set<Integer> listRoundId);

	List<InterviewerAnalyticsQO> getInterviewerAnalytics(Set<Integer> listRoundId);

	List<InterviewQO> getInterviewsByDriveIdAndInterviewer(Integer driveId, String interviewer);
}
