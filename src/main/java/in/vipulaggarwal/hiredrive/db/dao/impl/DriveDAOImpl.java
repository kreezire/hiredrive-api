package in.vipulaggarwal.hiredrive.db.dao.impl;

import in.vipulaggarwal.hiredrive.db.dao.DriveDAO;
import in.vipulaggarwal.hiredrive.db.qo.DriveQO;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import in.vipulaggarwal.hiredrive.exceptions.ResourceAddException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.List;

public class DriveDAOImpl extends AbstractDAO implements DriveDAO {
	private static final Logger logger = LoggerFactory.getLogger(BasicUserDAOImpl.class);

	public Integer addDrive(DriveQO driveQO) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("name", driveQO.getName());
			namedParameters.addValue("status", driveQO.getStatus());
			namedParameters.addValue("createduserid", driveQO.getCreatedUserId());
			KeyHolder keyHolder = new GeneratedKeyHolder();
			int affectedRowCount = this.namedParameterJdbcTemplate.update(
					"INSERT INTO DRIVE (NAME, STATUS, CREATED_USER_ID) VALUES (:name, :status, :createduserid);",
					namedParameters, keyHolder);
			if (affectedRowCount == 1) {
				return keyHolder.getKey().intValue();
			} else {
				logger.error("Drive details not added. driveQO={}", driveQO);
				throw new ResourceAddException("Drive cannot be added to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while adding driveQO={}", driveQO, var5);
			throw new HDException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem adding driveQO={} to DB", driveQO, var6);
			throw new HDException(var6);
		}
	}

	public DriveQO get(Integer id) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("driveId", id);
			return this.namedParameterJdbcTemplate.queryForObject("SELECT * FROM DRIVE WHERE ID  = :driveId",
					namedParameters, new BeanPropertyRowMapper<>(DriveQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting drive by id={} from DB", id, var4);
			throw new HDException(var4);
		}
	}

	public Integer update(DriveQO driveQO) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("id", driveQO.getId());
			namedParameters.addValue("name", driveQO.getName());
			namedParameters.addValue("status", driveQO.getStatus());
			namedParameters.addValue("modifiedUserId", driveQO.getModifiedUserId());
			int affectedRowCount = this.namedParameterJdbcTemplate.update("UPDATE DRIVE SET "
					+ "NAME = :name, STATUS = :status, MODIFIED_USER_ID = :modifiedUserId" + " WHERE ID = :id;",
					namedParameters);
			if (affectedRowCount == 1) {
				return driveQO.getId();
			} else {
				logger.error("Drive details not updated. driveQO={}", driveQO);
				throw new ResourceAddException("Drive cannot be updated to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while updating driveQO={}", driveQO, var5);
			throw new ResourceAddException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem updating driveQO={} to DB", driveQO, var6);
			throw new ResourceAddException("Data access exception.");
		}
	}

	public List<DriveQO> getDrivesByCreator(String username) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("createdUserId", username);
			return this.namedParameterJdbcTemplate.query(
					"SELECT * FROM DRIVE WHERE CREATED_USER_ID  = :createdUserId ORDER BY CREATED_TIME DESC",
					namedParameters, new BeanPropertyRowMapper<>(DriveQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting drive by username={} from DB", username, var4);
			throw new HDException(var4);
		}
	}

	public List<DriveQO> getAllDrives(String username) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("username", username);
			return this.namedParameterJdbcTemplate.query(
					"SELECT * FROM DRIVE WHERE ID IN (SELECT DRIVE_ID FROM INTERVIEWER WHERE EMAIL_ID = :username) "
							+ "OR CREATED_USER_ID = :username ORDER BY CREATED_TIME DESC",
					namedParameters, new BeanPropertyRowMapper<>(DriveQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting drive by username={} from DB", username, var4);
			throw new HDException(var4);
		}
	}

	public List<DriveQO> getMyTeamsDrives(String username) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("username", username);
			return this.namedParameterJdbcTemplate.query(
					"SELECT * FROM DRIVE WHERE ID IN (SELECT DRIVE_ID FROM INTERVIEWER WHERE EMAIL_ID = :username) "
							+ "AND CREATED_USER_ID != :username ORDER BY CREATED_TIME DESC",
					namedParameters, new BeanPropertyRowMapper<>(DriveQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting drive by username={} from DB", username, var4);
			throw new HDException(var4);
		}
	}
}
