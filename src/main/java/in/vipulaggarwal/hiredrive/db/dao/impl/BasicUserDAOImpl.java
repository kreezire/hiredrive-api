package in.vipulaggarwal.hiredrive.db.dao.impl;

import in.vipulaggarwal.hiredrive.db.dao.BasicUserDAO;
import in.vipulaggarwal.hiredrive.db.qo.BasicUserQO;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class BasicUserDAOImpl extends AbstractDAO implements BasicUserDAO {
	private static final Logger logger = LoggerFactory.getLogger(BasicUserDAOImpl.class);

	public Integer addUser(BasicUserQO basicUserQO) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("firstname", basicUserQO.getFirstName());
			namedParameters.addValue("lastname", basicUserQO.getLastName());
			namedParameters.addValue("emailid", basicUserQO.getEmailId().toLowerCase());
			KeyHolder keyHolder = new GeneratedKeyHolder();
			int affectedRowCount = this.namedParameterJdbcTemplate.update(
					"INSERT INTO USER (FIRST_NAME, LAST_NAME, EMAIL_ID) VALUES (:firstname, :lastname, :emailid);",
					namedParameters, keyHolder);
			if (affectedRowCount == 1) {
				return keyHolder.getKey().intValue();
			} else {
				logger.error("User details not added. basicUserQO={}", basicUserQO);
				throw new RuntimeException("User cannot be added to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while adding basicUserQO={}", basicUserQO, var5);
			throw new RuntimeException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem adding basicUserQO={} to DB", basicUserQO, var6);
			throw new RuntimeException(var6);
		}
	}

	public BasicUserQO getUserByUsername(String username) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("emailId", username);
			return this.namedParameterJdbcTemplate.queryForObject("SELECT * FROM USER WHERE EMAIL_ID  = :emailId",
					namedParameters, new BeanPropertyRowMapper<>(BasicUserQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting user details by userId={} from DB", username, var4);
			throw new HDException(var4);
		}
	}
}
