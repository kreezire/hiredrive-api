package in.vipulaggarwal.hiredrive.db.dao.impl;

import in.vipulaggarwal.hiredrive.db.dao.InterviewDAO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewCandidateRoundQO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewQO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewerAnalyticsQO;
import in.vipulaggarwal.hiredrive.db.qo.RoundAnalyticsQO;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import in.vipulaggarwal.hiredrive.exceptions.ResourceAddException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.List;
import java.util.Set;

public class InterviewDAOImpl extends AbstractDAO implements InterviewDAO {

	private static final Logger logger = LoggerFactory.getLogger(InterviewDAOImpl.class);
	private static final String SQL_INSERT_STATEMENT = "INSERT INTO INTERVIEW (ROUND_ID, CANDIDATE_ID, INTERVIEWER, STATUS, CREATED_USER_ID)"
			+ "VALUES (:roundId, :candidateId, :interviewer, :status, :createdUserId);";
	private static final String SQL_GET_BY_ID = "SELECT * FROM INTERVIEW WHERE ID  = :id;";
	private static final String SQL_GET_BY_CANDIDATE_ID_AND_STATUS = "SELECT * FROM INTERVIEW WHERE CANDIDATE_ID = :candidateId AND STATUS = :status;";
	private static final String SQL_UPDATE_STATEMENT = "UPDATE INTERVIEW SET "
			+ "STATUS = :status, INTERVIEWER = :interviewer, DURATION = :duration, START_TIME = :startTime, CANDIDATE_ID = :candidateId, "
			+ "ROUND_ID = :roundId, RATING = :rating, SUBMIT_TIME = :submitTime, FEEDBACK = :feedback, FEEDBACK_DRAFT = :feedbackDraft, MODIFIED_USER_ID = :modifiedUserId"
			+ " WHERE ID = :id;";
	private static final String SQL_GET_BY_CANDIDATE_ID_AND_ROUND_ID = "SELECT * FROM INTERVIEW WHERE CANDIDATE_ID = :candidateId AND ROUND_ID = :roundId;";

	private static final String SQL_GET_COUNT_BY_ROUND_ID = "SELECT COUNT(*) FROM INTERVIEW WHERE ROUND_ID = :roundId;";

	private static final String SQL_GET_BY_CANDIDATE_ID = "SELECT * FROM INTERVIEW WHERE CANDIDATE_ID = :candidateId;";

	private static final String SQL_GET_BY_INTERVIEWER = "SELECT * FROM INTERVIEW WHERE INTERVIEWER = :interviewer;";

	private static final String SQL_GET_ROUND_ANALYTICS = "SELECT ROUND_ID, COUNT(ROUND_ID) as COUNT FROM INTERVIEW where ROUND_ID in (:rounds) and status in (\"DONE\", \"ONGOING\") group by ROUND_ID ORDER BY COUNT DESC;";

	private static final String SQL_GET_INTERVIEWER_ANALYTICS = "SELECT INTERVIEWER, COUNT(INTERVIEWER) as COUNT FROM INTERVIEW where ROUND_ID in (:rounds)  group by INTERVIEWER ORDER BY COUNT DESC;";

	private static final String SQL_GET_BY_DRIVE_ID_AND_INTERVIEWER = "SELECT * FROM INTERVIEW WHERE INTERVIEWER = :interviewer AND CANDIDATE_ID in (SELECT id FROM CANDIDATE where DRIVE_ID = :driveId);";

	private MapSqlParameterSource getNamedParameterForAdd(InterviewQO interviewQO) {
		MapSqlParameterSource namedParameter = new MapSqlParameterSource();
		namedParameter.addValue("roundId", interviewQO.getRoundId());
		namedParameter.addValue("candidateId", interviewQO.getCandidateId());
		namedParameter.addValue("interviewer", interviewQO.getInterviewer());
		namedParameter.addValue("status", interviewQO.getStatus());
		namedParameter.addValue("createdUserId", interviewQO.getCreatedUserId());
		return namedParameter;
	}

	public Integer add(InterviewQO interviewQO) {

		try {
			MapSqlParameterSource namedParameters = getNamedParameterForAdd(interviewQO);
			KeyHolder keyHolder = new GeneratedKeyHolder();
			int affectedRowCount = this.namedParameterJdbcTemplate.update(SQL_INSERT_STATEMENT, namedParameters,
					keyHolder);
			if (affectedRowCount == 1) {
				return keyHolder.getKey().intValue();
			} else {
				logger.error("Interview details not added. interviewQO={}", interviewQO);
				throw new ResourceAddException("Interview cannot be added to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while adding interviewQO={}", interviewQO, var5);
			throw new HDException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem adding interviewQO={} to DB", interviewQO, var6);
			throw new HDException(var6);
		}

	}

	public InterviewQO get(Integer id) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
			return this.namedParameterJdbcTemplate.queryForObject(SQL_GET_BY_ID, namedParameters,
					new BeanPropertyRowMapper<>(InterviewQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interviewQO by id={} from DB", id, var4);
			throw new HDException(var4);
		}
	}

	public List<InterviewQO> getByCandidateIdAndStatus(Integer id, String status) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("candidateId", id);
			namedParameters.addValue("status", status);
			return this.namedParameterJdbcTemplate.query(SQL_GET_BY_CANDIDATE_ID_AND_STATUS, namedParameters,
					new BeanPropertyRowMapper<>(InterviewQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interview by candidateId={} And status={} from DB", id, status, var4);
			throw new HDException(var4);
		}
	}

	private MapSqlParameterSource getNamedParameterForUpdate(InterviewQO interviewQO) {
		MapSqlParameterSource namedParameter = new MapSqlParameterSource();
		namedParameter.addValue("id", interviewQO.getId());
		namedParameter.addValue("status", interviewQO.getStatus());
		namedParameter.addValue("interviewer", interviewQO.getInterviewer());
		namedParameter.addValue("startTime", interviewQO.getStartTime());
		namedParameter.addValue("candidateId", interviewQO.getCandidateId());
		namedParameter.addValue("roundId", interviewQO.getRoundId());
		namedParameter.addValue("rating", interviewQO.getRating());
		namedParameter.addValue("submitTime", interviewQO.getSubmitTime());
		namedParameter.addValue("feedback", interviewQO.getFeedback());
		namedParameter.addValue("modifiedUserId", interviewQO.getModifiedUserId());
		namedParameter.addValue("feedbackDraft", interviewQO.getFeedbackDraft());
		namedParameter.addValue("duration", interviewQO.getDuration());
		return namedParameter;
	}

	public Integer update(InterviewQO interviewQO) {
		try {
			MapSqlParameterSource namedParameters = getNamedParameterForUpdate(interviewQO);
			int affectedRowCount = this.namedParameterJdbcTemplate.update(SQL_UPDATE_STATEMENT, namedParameters);
			if (affectedRowCount == 1) {
				return interviewQO.getId();
			} else {
				logger.error("Interview details not updated. InterviewQO={}", interviewQO);
				throw new ResourceAddException("Interview cannot be updated to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while updating InterviewQO={}", interviewQO, var5);
			throw new ResourceAddException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem updating InterviewQO={} to DB", interviewQO, var6);
			throw new ResourceAddException("Data access exception.");
		}
	}

	public InterviewQO getByCandidateIdAndRoundId(Integer candidateId, Integer roundId) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("candidateId", candidateId);
			namedParameters.addValue("roundId", roundId);
			return this.namedParameterJdbcTemplate.queryForObject(SQL_GET_BY_CANDIDATE_ID_AND_ROUND_ID, namedParameters,
					new BeanPropertyRowMapper<>(InterviewQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interview by candidateId={} And roundId={} from DB", candidateId, roundId,
					var4);
			throw new HDException(var4);
		}
	}

	public List<InterviewQO> getInterviewByCandidateId(Integer candidateId) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("candidateId", candidateId);
			return this.namedParameterJdbcTemplate.query(SQL_GET_BY_CANDIDATE_ID, namedParameters,
					new BeanPropertyRowMapper<>(InterviewQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interview by candidateId={} from DB", candidateId, var4);
			throw new HDException(var4);
		}
	}

	public List<InterviewQO> getInterviewAssignedToMe(String username) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("interviewer", username);
			return this.namedParameterJdbcTemplate.query(SQL_GET_BY_INTERVIEWER, namedParameters,
					new BeanPropertyRowMapper<>(InterviewQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interview by interviewer={} from DB", username, var4);
			throw new HDException(var4);
		}
	}

	public List<InterviewCandidateRoundQO> getInterviewCandidateRoundDetailsAssignedToMe(String username) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("interviewer", username);
			List<InterviewCandidateRoundQO> t = this.namedParameterJdbcTemplate.query(
					"SELECT INTERVIEW.ID AS INTERVIEW_ID, INTERVIEW.STATUS AS INTERVIEW_STATUS, ROUND.NAME AS ROUND_NAME, "
							+ "INTERVIEW.CANDIDATE_ID AS CANDIDATE_ID, CANDIDATE.NAME AS CANDIDATE_NAME FROM INTERVIEW "
							+ "INNER JOIN ROUND ON INTERVIEW.ROUND_ID = ROUND.ID INNER JOIN CANDIDATE ON INTERVIEW.CANDIDATE_ID = CANDIDATE.ID  "
							+ "WHERE INTERVIEW.INTERVIEWER = :interviewer;",
					namedParameters, new BeanPropertyRowMapper<>(InterviewCandidateRoundQO.class));
			return t;
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interview by interviewer={} from DB", username, var4);
			throw new HDException(var4);
		}
	}

	public Integer getCountByRoundId(Integer roundId) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("roundId", roundId);

			return this.namedParameterJdbcTemplate.queryForObject(SQL_GET_COUNT_BY_ROUND_ID, namedParameters,
					Integer.class);
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting count of interviews roundId={} from DB", roundId, var4);
			throw new HDException(var4);
		}
	}

	public List<RoundAnalyticsQO> getRoundAnalytics(Set<Integer> listRoundId) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("rounds", listRoundId);
			return this.namedParameterJdbcTemplate.query(SQL_GET_ROUND_ANALYTICS, namedParameters,
					new BeanPropertyRowMapper<>(RoundAnalyticsQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting round analytics for roundIds={} from DB", listRoundId, var4);
			throw new HDException(var4);
		}
	}

	public List<InterviewerAnalyticsQO> getInterviewerAnalytics(Set<Integer> listRoundId) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("rounds", listRoundId);
			return this.namedParameterJdbcTemplate.query(SQL_GET_INTERVIEWER_ANALYTICS, namedParameters,
					new BeanPropertyRowMapper<>(InterviewerAnalyticsQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interviewer analytics for roundIds={} from DB", listRoundId, var4);
			throw new HDException(var4);
		}
	}

	public List<InterviewQO> getInterviewsByDriveIdAndInterviewer(Integer driveId, String interviewer) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("driveId", driveId);
			namedParameters.addValue("interviewer", interviewer);
			return this.namedParameterJdbcTemplate.query(SQL_GET_BY_DRIVE_ID_AND_INTERVIEWER, namedParameters,
					new BeanPropertyRowMapper<>(InterviewQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interviews by driveId={} and interviewer={} from DB", driveId, interviewer,
					var4);
			throw new HDException(var4);
		}
	}

}
