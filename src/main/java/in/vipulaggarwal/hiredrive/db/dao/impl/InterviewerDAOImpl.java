package in.vipulaggarwal.hiredrive.db.dao.impl;

import in.vipulaggarwal.hiredrive.db.dao.InterviewerDAO;
import in.vipulaggarwal.hiredrive.db.qo.InterviewerQO;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import in.vipulaggarwal.hiredrive.exceptions.ResourceAddException;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class InterviewerDAOImpl extends AbstractDAO implements InterviewerDAO {

	private static final Logger logger = LoggerFactory.getLogger(InterviewerDAOImpl.class);
	private static final String SQL_INSERT_STATEMENT = "INSERT INTO INTERVIEWER (EMAIL_ID, DRIVE_ID, CREATED_USER_ID)"
			+ "VALUES (:emailId, :driveId, :createdUserId);";
	private static final String SQL_GET_BY_ID = "SELECT * FROM INTERVIEWER WHERE ID  = :id;";
	private static final String SQL_GET_BY_DRIVE_ID_AND_EMAIL_ID = "SELECT * FROM INTERVIEWER WHERE DRIVE_ID  = :driveId and EMAIL_ID = :emailId;";
	private static final String SQL_UPDATE_STATEMENT = "UPDATE INTERVIEWER SET "
			+ "EMAIL_ID = :emailId, DRIVE_ID = :driveId, MODIFIED_USER_ID = :modifiedUserId WHERE ID = :id;";
	private static final String SQL_DELETE_STATEMENT = "DELETE FROM INTERVIEWER where ID=:id";
	private static final String SQL_GET_BY_DRIVE_ID = "SELECT * FROM INTERVIEWER WHERE DRIVE_ID = :driveId;";

	private MapSqlParameterSource getNamedParameterForAdd(InterviewerQO interviewerQO) {
		MapSqlParameterSource namedParameter = new MapSqlParameterSource();
		namedParameter.addValue("emailId", interviewerQO.getEmailId());
		namedParameter.addValue("driveId", interviewerQO.getDriveId());
		namedParameter.addValue("createdUserId", interviewerQO.getCreatedUserId());
		return namedParameter;
	}

	public Integer add(InterviewerQO interviewerQO) {

		try {
			MapSqlParameterSource namedParameters = getNamedParameterForAdd(interviewerQO);
			KeyHolder keyHolder = new GeneratedKeyHolder();
			int affectedRowCount = this.namedParameterJdbcTemplate.update(SQL_INSERT_STATEMENT, namedParameters,
					keyHolder);
			if (affectedRowCount == 1) {
				return keyHolder.getKey().intValue();
			} else {
				logger.error("Interviewer details not added. interviewerQO={}", interviewerQO);
				throw new ResourceAddException("Interviewer cannot be added to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while adding interviewerQO={}", interviewerQO, var5);
			throw new HDException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem adding interviewerQO={} to DB", interviewerQO, var6);
			throw new HDException(var6);
		}

	}

	public InterviewerQO get(Integer id) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
			return this.namedParameterJdbcTemplate.queryForObject(SQL_GET_BY_ID, namedParameters,
					new BeanPropertyRowMapper<>(InterviewerQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interviewerQO by id={} from DB", id, var4);
			throw new HDException(var4);
		}
	}

	public InterviewerQO getByDriveIdAndEmailId(Integer driveId, String emailId) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("driveId", driveId);
			namedParameters.addValue("emailId", emailId);
			return this.namedParameterJdbcTemplate.queryForObject(SQL_GET_BY_DRIVE_ID_AND_EMAIL_ID, namedParameters,
					new BeanPropertyRowMapper<>(InterviewerQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interviewerQO by driveId={} and emailId={} from DB", driveId, emailId, var4);
			throw new HDException(var4);
		}
	}

	private MapSqlParameterSource getNamedParameterForUpdate(InterviewerQO interviewerQO) {
		MapSqlParameterSource namedParameter = new MapSqlParameterSource();
		namedParameter.addValue("id", interviewerQO.getId());
		namedParameter.addValue("emailId", interviewerQO.getEmailId());
		namedParameter.addValue("driveId", interviewerQO.getDriveId());
		namedParameter.addValue("modifiedUserId", interviewerQO.getModifiedUserId());
		return namedParameter;
	}

	public Integer update(InterviewerQO interviewerQO) {
		try {
			MapSqlParameterSource namedParameters = getNamedParameterForUpdate(interviewerQO);
			int affectedRowCount = this.namedParameterJdbcTemplate.update(SQL_UPDATE_STATEMENT, namedParameters);
			if (affectedRowCount == 1) {
				return interviewerQO.getId();
			} else {
				logger.error("Interviewer details not updated. InterviewerQO={}", interviewerQO);
				throw new ResourceAddException("Interviewer cannot be updated to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while updating InterviewerQO={}", interviewerQO, var5);
			throw new ResourceAddException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem updating InterviewerQO={} to DB", interviewerQO, var6);
			throw new ResourceAddException("Data access exception.");
		}
	}

	public boolean delete(Integer id) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("id", id);
			int affectedRowCount = namedParameterJdbcTemplate.update(SQL_DELETE_STATEMENT, namedParameters);
			return affectedRowCount == 1;
		} catch (DataAccessException e) {
			logger.error("Error while deleting Interviewer", e);
			throw new HDException(e);
		}
	}

	public List<InterviewerQO> getInterviewersByDriveId(@NotNull Integer driveId) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("driveId", driveId);
			return this.namedParameterJdbcTemplate.query(SQL_GET_BY_DRIVE_ID, namedParameters,
					new BeanPropertyRowMapper<>(InterviewerQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting interviewer by driveId={} from DB", driveId, var4);
			throw new HDException(var4);
		}
	}

}
