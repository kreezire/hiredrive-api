package in.vipulaggarwal.hiredrive.db.dao;

import in.vipulaggarwal.hiredrive.db.qo.DriveQO;

import java.util.List;

public interface DriveDAO {
	Integer addDrive(DriveQO driveQO);

	DriveQO get(Integer id);

	Integer update(DriveQO driveQO);

	List<DriveQO> getDrivesByCreator(String username);

	List<DriveQO> getAllDrives(String username);

	List<DriveQO> getMyTeamsDrives(String username);
}
