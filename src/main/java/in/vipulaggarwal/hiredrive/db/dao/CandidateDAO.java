package in.vipulaggarwal.hiredrive.db.dao;

import in.vipulaggarwal.hiredrive.db.qo.CandidateQO;
import in.vipulaggarwal.hiredrive.db.qo.TalentPartnerAnalyticsQO;

import java.util.List;

public interface CandidateDAO {
	Integer add(CandidateQO candidateQO);

	CandidateQO get(Integer id);

	Integer update(CandidateQO candidateQO);

	List<CandidateQO> getCandidateListByDriveId(Integer driveId);

	List<CandidateQO> getNonDriveCandidatesByCreatedBy(String createdBy);
	
	List<TalentPartnerAnalyticsQO> getTalentPartnerAnalyticsForDrive(Integer driveId);

}
