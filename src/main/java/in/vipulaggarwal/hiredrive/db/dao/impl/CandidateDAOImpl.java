package in.vipulaggarwal.hiredrive.db.dao.impl;

import in.vipulaggarwal.hiredrive.db.dao.CandidateDAO;
import in.vipulaggarwal.hiredrive.db.qo.CandidateQO;
import in.vipulaggarwal.hiredrive.db.qo.TalentPartnerAnalyticsQO;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import in.vipulaggarwal.hiredrive.exceptions.ResourceAddException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.List;

public class CandidateDAOImpl extends AbstractDAO implements CandidateDAO {
	private static final Logger logger = LoggerFactory.getLogger(CandidateDAOImpl.class);

	private static final String SQL_TALENT_PARTNER_ANALYTICS = "SELECT TALENT_PARTNER, count(*) as total, sum(case when status = 'SELECTED' then 1 else 0 end) AS selected  FROM kodex.candidate where drive_id= :driveId group by TALENT_PARTNER;";
	public Integer add(CandidateQO candidateQO) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("name", candidateQO.getName());
			namedParameters.addValue("phone", candidateQO.getPhone());
			namedParameters.addValue("emailId", candidateQO.getEmailId());
			namedParameters.addValue("driveid", candidateQO.getDriveId());
			namedParameters.addValue("resume", candidateQO.getResumePath());
			namedParameters.addValue("status", candidateQO.getStatus());
			namedParameters.addValue("createduserid", candidateQO.getCreatedUserId());
			KeyHolder keyHolder = new GeneratedKeyHolder();
			int affectedRowCount = this.namedParameterJdbcTemplate.update(
					"INSERT INTO CANDIDATE "
							+ "(NAME, PHONE, EMAIL_ID, RESUME_PATH, DRIVE_ID, STATUS, CREATED_USER_ID) " + "VALUES "
							+ "(:name, :phone, :emailId, :resume, :driveid, :status, :createduserid);",
					namedParameters, keyHolder);
			if (affectedRowCount == 1) {
				return keyHolder.getKey().intValue();
			} else {
				logger.error("Candidate details not added. candidateQO={}", candidateQO);
				throw new ResourceAddException("Candidate cannot be added to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while adding candidateQO={}", candidateQO, var5);
			throw new ResourceAddException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem adding candidateQO={} to DB", candidateQO, var6);
			throw new ResourceAddException("Data access exception.");
		}
	}

	public CandidateQO get(Integer id) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("candidateId", id);
			return this.namedParameterJdbcTemplate.queryForObject("SELECT * FROM CANDIDATE WHERE ID  = :candidateId",
					namedParameters, new BeanPropertyRowMapper<>(CandidateQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting candidate by id={} from DB", id, var4);
			throw new HDException(var4);
		}
	}

	public Integer update(CandidateQO candidateQO) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("id", candidateQO.getId());
			namedParameters.addValue("name", candidateQO.getName());
			namedParameters.addValue("phone", candidateQO.getPhone());
			namedParameters.addValue("emailId", candidateQO.getEmailId());
			namedParameters.addValue("driveid", candidateQO.getDriveId());
			namedParameters.addValue("resume", candidateQO.getResumePath());
			namedParameters.addValue("status", candidateQO.getStatus());
			namedParameters.addValue("gender", candidateQO.getGender());
			namedParameters.addValue("talentPartner", candidateQO.getTalentPartner());
			namedParameters.addValue("yoe", candidateQO.getYoe());
			namedParameters.addValue("skills", candidateQO.getSkills());
			namedParameters.addValue("modifiedUserId", candidateQO.getModifiedUserId());
			
			int affectedRowCount = this.namedParameterJdbcTemplate.update("UPDATE CANDIDATE SET "
					+ "NAME = :name, PHONE = :phone, EMAIL_ID = :emailId, RESUME_PATH = :resume, DRIVE_ID = :driveid, STATUS = :status,"
					+ " GENDER = :gender, TALENT_PARTNER = :talentPartner, YOE = :yoe, SKILLS = :skills,"
					+ " MODIFIED_USER_ID = :modifiedUserId" + " WHERE ID = :id;", namedParameters);
			if (affectedRowCount == 1) {
				return candidateQO.getId();
			} else {
				logger.error("Candidate details not updated. candidateQO={}", candidateQO);
				throw new ResourceAddException("Candidate cannot be updated to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while updating candidateQO={}", candidateQO, var5);
			throw new ResourceAddException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem updating candidateQO={} to DB", candidateQO, var6);
			throw new ResourceAddException("Data access exception.");
		}
	}

	@Override
	public List<CandidateQO> getCandidateListByDriveId(Integer driveId) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("driveId", driveId);
			return this.namedParameterJdbcTemplate.query("SELECT * FROM CANDIDATE WHERE DRIVE_ID = :driveId",
					namedParameters, new BeanPropertyRowMapper<>(CandidateQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting Candidates by driveId={} from DB", driveId, var4);
			throw new HDException(var4);
		}
	}

	public List<CandidateQO> getNonDriveCandidatesByCreatedBy(String createdBy) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("createdUserId", createdBy);
			return this.namedParameterJdbcTemplate.query("SELECT * FROM CANDIDATE WHERE CREATED_USER_ID = :createdUserId AND DRIVE_ID is NULL ORDER BY CREATED_TIME DESC;",
					namedParameters, new BeanPropertyRowMapper<>(CandidateQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting Candidates by createdBy={} from DB", createdBy, var4);
			throw new HDException(var4);
		}
	}
	
	public List<TalentPartnerAnalyticsQO> getTalentPartnerAnalyticsForDrive(Integer driveId) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("driveId", driveId);
			return this.namedParameterJdbcTemplate.query(SQL_TALENT_PARTNER_ANALYTICS,
					namedParameters, new BeanPropertyRowMapper<>(TalentPartnerAnalyticsQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting talent partner analytics by driveId={} from DB", driveId, var4);
			throw new HDException(var4);
		}
	}
}
