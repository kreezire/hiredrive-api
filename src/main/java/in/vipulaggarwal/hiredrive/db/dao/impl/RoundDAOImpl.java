package in.vipulaggarwal.hiredrive.db.dao.impl;

import in.vipulaggarwal.hiredrive.db.dao.RoundDAO;
import in.vipulaggarwal.hiredrive.db.qo.RoundQO;
import in.vipulaggarwal.hiredrive.enums.EntityType;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import in.vipulaggarwal.hiredrive.exceptions.ResourceAddException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.ArrayList;
import java.util.List;

public class RoundDAOImpl extends AbstractDAO implements RoundDAO {
	private static final Logger logger = LoggerFactory.getLogger(BasicUserDAOImpl.class);
	private static final String SQL_INSERT_STATEMENT = "INSERT INTO ROUND (NAME, ENTITY_ID, ENTITY_TYPE, REQUIRED, CREATED_USER_ID) VALUES (:name, :entityId, :entityType, :required, :createdUserId);";
	private static final String SQL_UPDATE_STATEMENT = "UPDATE ROUND SET "
			+ "NAME = :name, ENTITY_ID = :entityId, ENTITY_TYPE = :entityType, REQUIRED = :required, "
			+ " MODIFIED_USER_ID = :modifiedUserId" + " WHERE ID = :id;";
	private static final String SQL_DELETE_ROUND = "DELETE FROM ROUND where ID=:roundId";

	@Override
	public Integer addRound(RoundQO roundQO) {
		try {
			MapSqlParameterSource namedParameters = getNamedParameterForAdd(roundQO, roundQO.getEntityId(),
					roundQO.getEntityType());
			KeyHolder keyHolder = new GeneratedKeyHolder();
			int affectedRowCount = this.namedParameterJdbcTemplate.update(SQL_INSERT_STATEMENT, namedParameters,
					keyHolder);
			if (affectedRowCount == 1) {
				return keyHolder.getKey().intValue();
			} else {
				logger.error("Round details not added. roundQO={}", roundQO);
				throw new ResourceAddException("Round cannot be added to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while adding roundQO={}", roundQO, var5);
			throw new HDException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem adding roundQO={} to DB", roundQO, var6);
			throw new HDException(var6);
		}
	}

	@Override
	public RoundQO get(Integer id) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("roundId", id);
			return this.namedParameterJdbcTemplate.queryForObject("SELECT * FROM ROUND WHERE ID  = :roundId",
					namedParameters, new BeanPropertyRowMapper<>(RoundQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting round by id={} from DB", id, var4);
			throw new HDException(var4);
		}
	}

	@Override
	public List<RoundQO> getRoundByEntityIdAndEntityType(Integer entityId, EntityType entityType) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource("entityid", entityId);
			namedParameters.addValue("entitytype", entityType.toString());
			return this.namedParameterJdbcTemplate.query(
					"SELECT * FROM ROUND WHERE ENTITY_ID = :entityid  AND ENTITY_TYPE = :entitytype", namedParameters,
					new BeanPropertyRowMapper<>(RoundQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting round by entityId={} And entityType={} from DB", entityId, entityType, var4);
			throw new HDException(var4);
		}
	}

	@Override
	public Boolean addRounds(List<RoundQO> roundQOs, Integer entityId, String entityType) {
		try {
			List<MapSqlParameterSource> namedParameters = new ArrayList<>();
			for (RoundQO roundQO : roundQOs) {
				MapSqlParameterSource namedParameter = getNamedParameterForAdd(roundQO, entityId, entityType);
				namedParameters.add(namedParameter);
			}
			int[] affectedCount = namedParameterJdbcTemplate.batchUpdate(SQL_INSERT_STATEMENT,
					namedParameters.toArray(new MapSqlParameterSource[0]));
			return (affectedCount.length == roundQOs.size());
		} catch (Exception e) {
			logger.error("Error while inserting roundQOs in rounds table", e);
			throw new HDException(e);
		}
	}

	public Integer update(RoundQO roundQO) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("id", roundQO.getId());
			namedParameters.addValue("name", roundQO.getName());
			namedParameters.addValue("entityId", roundQO.getEntityId());
			namedParameters.addValue("entityType", roundQO.getEntityType());
			namedParameters.addValue("required", roundQO.isRequired());
			namedParameters.addValue("modifiedUserId", roundQO.getModifiedUserId());
			int affectedRowCount = this.namedParameterJdbcTemplate.update(SQL_UPDATE_STATEMENT, namedParameters);
			if (affectedRowCount == 1) {
				return roundQO.getId();
			} else {
				logger.error("Round details not updated. RoundQO={}", roundQO);
				throw new ResourceAddException("Round cannot be updated to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while updating RoundQO={}", roundQO, var5);
			throw new ResourceAddException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem updating RoundQO={} to DB", roundQO, var6);
			throw new ResourceAddException("Data access exception.");
		}
	}

	private MapSqlParameterSource getNamedParameterForAdd(RoundQO roundQO, Integer entityId, String entityType) {
		MapSqlParameterSource namedParameter = new MapSqlParameterSource();
		namedParameter.addValue("name", roundQO.getName());
		namedParameter.addValue("entityId", entityId);
		namedParameter.addValue("entityType", entityType);
		namedParameter.addValue("required", roundQO.isRequired());
		namedParameter.addValue("createdUserId", roundQO.getCreatedUserId());
		return namedParameter;
	}

	public boolean delete(Integer id) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("roundId", id);
			int affectedRowCount = namedParameterJdbcTemplate.update(SQL_DELETE_ROUND, namedParameters);
			return affectedRowCount == 1;
		} catch (DataAccessException e) {
			logger.error("Error while deleting round", e);
			throw new HDException(e);
		}
	}
}
