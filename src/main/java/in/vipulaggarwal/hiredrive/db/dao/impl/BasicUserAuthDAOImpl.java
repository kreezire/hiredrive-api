package in.vipulaggarwal.hiredrive.db.dao.impl;

import in.vipulaggarwal.hiredrive.db.dao.BasicUserAuthDAO;
import in.vipulaggarwal.hiredrive.db.qo.BasicUserAuthQO;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class BasicUserAuthDAOImpl extends AbstractDAO implements BasicUserAuthDAO {
	private static final Logger logger = LoggerFactory.getLogger(BasicUserAuthDAOImpl.class);

	public Integer addUserAuth(BasicUserAuthQO basicUserAuthQO) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("emailid", basicUserAuthQO.getEmailId());
			namedParameters.addValue("password", basicUserAuthQO.getPassword());
			KeyHolder keyHolder = new GeneratedKeyHolder();
			int affectedRowCount = this.namedParameterJdbcTemplate.update(
					"INSERT INTO BASIC_USER_AUTH (EMAIL_ID, PASSWORD) VALUES (:emailid, :password);", namedParameters,
					keyHolder);
			if (affectedRowCount == 1) {
				return keyHolder.getKey().intValue();
			} else {
				logger.error("User Auth details not added. basicUserAuthQO={}", basicUserAuthQO);
				throw new RuntimeException("User Auth cannot be added to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while adding basicUserAuthQO={}", basicUserAuthQO, var5);
			throw new RuntimeException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem adding basicUserAuthQO={} to DB", basicUserAuthQO, var6);
			throw new RuntimeException(var6);
		}

	}

	public Integer updateUserAuth(BasicUserAuthQO basicUserAuthQO) {
		try {
			MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			namedParameters.addValue("emailid", basicUserAuthQO.getEmailId());
			namedParameters.addValue("password", basicUserAuthQO.getPassword());
			int affectedRowCount = this.namedParameterJdbcTemplate.update(
					"UPDATE BASIC_USER_AUTH SET PASSWORD=:password WHERE EMAIL_ID = :emailid;", namedParameters);
			if (affectedRowCount == 1) {
				return basicUserAuthQO.getId();
			} else {
				logger.error("User Auth details not added. basicUserAuthQO={}", basicUserAuthQO);
				throw new RuntimeException("User Auth cannot be added to DB.");
			}
		} catch (DataIntegrityViolationException var5) {
			logger.error("Error while adding basicUserAuthQO={}", basicUserAuthQO, var5);
			throw new RuntimeException("Data Integrity Violation");
		} catch (DataAccessException var6) {
			logger.error("Problem adding basicUserAuthQO={} to DB", basicUserAuthQO, var6);
			throw new RuntimeException(var6);
		}

	}

	public BasicUserAuthQO getAuthDetailsByUserId(String userId) {
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("emailId", userId);
			return this.namedParameterJdbcTemplate.queryForObject(
					"SELECT * FROM BASIC_USER_AUTH WHERE EMAIL_ID  = :emailId", namedParameters,
					new BeanPropertyRowMapper<>(BasicUserAuthQO.class));
		} catch (IncorrectResultSizeDataAccessException var3) {
			return null;
		} catch (DataAccessException var4) {
			logger.error("Problem getting user auth details by userId={} from DB", userId, var4);
			throw new HDException(var4);
		}
	}
}
