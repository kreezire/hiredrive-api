package in.vipulaggarwal.hiredrive.db.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public abstract class AbstractDAO {
    @Autowired
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate;

}
