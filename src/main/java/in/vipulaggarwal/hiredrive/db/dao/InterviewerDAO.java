package in.vipulaggarwal.hiredrive.db.dao;

import java.util.List;

import javax.validation.constraints.NotNull;

import in.vipulaggarwal.hiredrive.db.qo.InterviewerQO;

public interface InterviewerDAO {

	Integer add(InterviewerQO interviewerQO);

	InterviewerQO get(Integer id);
	
	InterviewerQO getByDriveIdAndEmailId(Integer driveId, String emailId);

	Integer update(InterviewerQO interviewerQO);

	boolean delete(Integer id);

	List<InterviewerQO> getInterviewersByDriveId(@NotNull Integer driveId);

}
