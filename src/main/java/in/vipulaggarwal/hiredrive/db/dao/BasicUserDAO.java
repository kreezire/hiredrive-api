package in.vipulaggarwal.hiredrive.db.dao;

import in.vipulaggarwal.hiredrive.db.qo.BasicUserQO;

public interface BasicUserDAO {
	Integer addUser(BasicUserQO basicUserQO);

	BasicUserQO getUserByUsername(String username);
}
