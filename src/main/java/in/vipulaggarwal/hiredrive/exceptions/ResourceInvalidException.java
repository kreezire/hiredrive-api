package in.vipulaggarwal.hiredrive.exceptions;

public class ResourceInvalidException extends HDException {
    public ResourceInvalidException(String message) {
        super(message);
    }
}
