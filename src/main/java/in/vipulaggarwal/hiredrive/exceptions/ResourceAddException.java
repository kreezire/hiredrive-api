package in.vipulaggarwal.hiredrive.exceptions;

public class ResourceAddException extends HDException {
    public ResourceAddException(String message) {
        super(message);
    }
}
