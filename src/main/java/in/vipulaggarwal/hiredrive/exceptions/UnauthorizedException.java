package in.vipulaggarwal.hiredrive.exceptions;

public class UnauthorizedException extends HDException {
    public UnauthorizedException(String message) {
        super(message);
    }
}
