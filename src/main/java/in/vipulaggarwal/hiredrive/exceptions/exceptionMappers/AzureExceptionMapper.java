package in.vipulaggarwal.hiredrive.exceptions.exceptionMappers;

import in.vipulaggarwal.hiredrive.exceptions.AzureException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class AzureExceptionMapper implements ExceptionMapper<AzureException> {
    private static Logger logger = LoggerFactory.getLogger(AzureExceptionMapper.class);

    public AzureExceptionMapper() {

    }

    @Context
    private HttpServletRequest request;

    @Autowired
    private ExceptionErrorMessageBuilder exceptionErrorMessageBuilder;

    public Response toResponse(AzureException exception) {
        String responseString = this.exceptionErrorMessageBuilder.getResponseJsonString(Response.Status.INTERNAL_SERVER_ERROR, StringUtils.isEmpty(exception.getMessage()) ? "An Internal Server Error Occurred." : exception.getMessage());
        Response.ResponseBuilder builder = Response.status(Response.Status.NOT_FOUND).entity(responseString);
        logger.error("Azure error " + this.request.getRequestURL(), exception);
        return builder.build();
    }
}
