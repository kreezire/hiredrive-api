package in.vipulaggarwal.hiredrive.exceptions.exceptionMappers;

import in.vipulaggarwal.hiredrive.exceptions.ResourceInvalidException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ResourceInvalidExceptionMapper implements ExceptionMapper<ResourceInvalidException> {
    private static Logger logger = LoggerFactory.getLogger(ResourceInvalidExceptionMapper.class);
    @Context
    private HttpServletRequest request;

    @Autowired
    ExceptionErrorMessageBuilder exceptionErrorMessageBuilder;


    public Response toResponse(ResourceInvalidException exception) {
        String responseString = this.exceptionErrorMessageBuilder.getResponseJsonString(Response.Status.BAD_REQUEST, exception.getMessage());
        Response.ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST).entity(responseString);
        logger.error("Bad request", exception);
        return builder.build();
    }
}
