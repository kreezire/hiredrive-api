package in.vipulaggarwal.hiredrive.exceptions.exceptionMappers;

import in.vipulaggarwal.hiredrive.exceptions.ResourceAddException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ResourceAddExceptionMapper implements ExceptionMapper<ResourceAddException> {

    private static Logger logger = LoggerFactory.getLogger(ResourceAddExceptionMapper.class);

    @Context
    private HttpServletRequest request;

    @Autowired
    private ExceptionErrorMessageBuilder exceptionErrorMessageBuilder;

    public Response toResponse(ResourceAddException exception) {
        logger.error(exception.getMessage());
        String responseString = this.exceptionErrorMessageBuilder.getResponseJsonString(Response.Status.INTERNAL_SERVER_ERROR, "Internal Server Error.");
        Response.ResponseBuilder builder = Response.status(Response.Status.EXPECTATION_FAILED).entity(responseString);
        logger.error("Expectation Failed " + this.request.getRequestURL(), exception);
        return builder.build();
    }
}
