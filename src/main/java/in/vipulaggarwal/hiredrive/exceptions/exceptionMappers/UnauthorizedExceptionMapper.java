package in.vipulaggarwal.hiredrive.exceptions.exceptionMappers;

import in.vipulaggarwal.hiredrive.exceptions.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UnauthorizedExceptionMapper implements ExceptionMapper<UnauthorizedException> {
    private static Logger logger = LoggerFactory.getLogger(UnauthorizedExceptionMapper.class);
    @Context
    private HttpServletRequest request;

    @Autowired
    ExceptionErrorMessageBuilder exceptionErrorMessageBuilder;


    public Response toResponse(UnauthorizedException exception) {
        String responseString = this.exceptionErrorMessageBuilder.getResponseJsonString(Response.Status.UNAUTHORIZED, exception.getMessage());
        Response.ResponseBuilder builder = Response.status(Response.Status.UNAUTHORIZED).entity(responseString);
        logger.error("Unauthorized request", exception);
        return builder.build();
    }
}
