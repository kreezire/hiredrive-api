package in.vipulaggarwal.hiredrive.exceptions.exceptionMappers;

import javax.ws.rs.core.Response;


public class ExceptionErrorMessageBuilder {
    public ExceptionErrorMessageBuilder() {
    }

    public String getResponseJsonString(Response.Status errorStatus, String errorMessage) {
        StringBuilder responseStringBuilder = new StringBuilder();
        responseStringBuilder.append("{");
        responseStringBuilder.append("\"error_code\": ");
        responseStringBuilder.append(errorStatus.getStatusCode());
        responseStringBuilder.append(",");
        responseStringBuilder.append("\"message\": \"");
        responseStringBuilder.append(errorMessage);
        responseStringBuilder.append("\"");
        responseStringBuilder.append("}");
        return responseStringBuilder.toString();
    }
}
