package in.vipulaggarwal.hiredrive.exceptions.exceptionMappers;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    private static Logger logger = LoggerFactory.getLogger(NotFoundExceptionMapper.class);

    @Context
    private HttpServletRequest request;

    @Autowired
    private ExceptionErrorMessageBuilder exceptionErrorMessageBuilder;

    public Response toResponse(NotFoundException exception) {
        String responseString = this.exceptionErrorMessageBuilder.getResponseJsonString(Response.Status.NOT_FOUND, StringUtils.isEmpty(exception.getMessage()) ? "Resource does not exist." : exception.getMessage());
        Response.ResponseBuilder builder = Response.status(Response.Status.NOT_FOUND).entity(responseString);
        logger.error("Not found " + this.request.getRequestURL(), exception);
        return builder.build();
    }
}
