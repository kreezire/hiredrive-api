package in.vipulaggarwal.hiredrive.exceptions;

public class HDException extends RuntimeException {
    public HDException() {
    }

    public HDException(String message) {
        super(message);
    }

    public HDException(String message, Throwable cause) {
        super(message, cause);
    }

    public HDException(Throwable cause) {
        super(cause);
    }

    public HDException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
