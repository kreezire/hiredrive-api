package in.vipulaggarwal.hiredrive.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

public class HDHttpException extends Exception {
    public static final String HTTP_STATUS = "HttpStatus";
    public static final String HTTP_MESSAGE = "HttpMessage";

    private HttpStatus httpStatus;
    private HttpHeaders httpHeaders;
    private String responseBodyAsString;

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public HttpHeaders getHttpHeaders() {
        return httpHeaders;
    }

    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    public String getResponseBodyAsString() {
        return responseBodyAsString;
    }

    public void setResponseBodyAsString(String responseBodyAsString) {
        this.responseBodyAsString = responseBodyAsString;
    }

    public HDHttpException() {
    }

    public HDHttpException(String message) {
        super(message);
    }

    public HDHttpException(String message, Throwable cause) {
        super(message, cause);
    }

    public HDHttpException(Throwable cause) {
        super(cause);
    }

    public HDHttpException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public HDHttpException(HttpStatus s, HttpHeaders responseHeaders, String responseBodyAsString) {
        super(new StringBuilder(HTTP_STATUS).append("=").append(s).append(",").append(HTTP_MESSAGE).append("=").append(responseBodyAsString).toString());
        this.httpStatus = s;
        this.httpHeaders = responseHeaders;
        this.responseBodyAsString = responseBodyAsString;
    }
}
