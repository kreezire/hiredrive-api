package in.vipulaggarwal.hiredrive.exceptions;

public class AzureException extends HDException {
    public AzureException(String message) {
        super(message);
    }
}
