package in.vipulaggarwal.hiredrive.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

public class HDHttpRetryException extends HDHttpException {

    public HDHttpRetryException(String message) {
        super(message);
    }

    public HDHttpRetryException(String message, Throwable cause) {
        super(message, cause);
    }

    public HDHttpRetryException(Throwable cause) {
        super(cause);
    }

    public HDHttpRetryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public HDHttpRetryException(HttpStatus s, HttpHeaders responseHeaders, String responseBodyAsString) {
        super(new StringBuilder(HTTP_STATUS).append("=").append(s).append(",").append(HTTP_MESSAGE).append("=").append(responseBodyAsString).toString());
    }
}

