package in.vipulaggarwal.hiredrive.aspect;

import in.vipulaggarwal.hiredrive.aspect.validator.InterviewValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = {InterviewValidator.class})
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidInterview {
    String message() default "Interview Details are not valid.";

    Class<?>[] groups() default {};

    String[] values() default {};

    Class<? extends Payload>[] payload() default {};
}
