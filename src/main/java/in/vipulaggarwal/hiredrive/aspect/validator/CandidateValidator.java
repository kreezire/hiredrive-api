package in.vipulaggarwal.hiredrive.aspect.validator;

import in.vipulaggarwal.hiredrive.aspect.ValidCandidate;
import in.vipulaggarwal.hiredrive.dto.CandidateDTO;
import org.apache.commons.lang3.StringUtils;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CandidateValidator implements ConstraintValidator<ValidCandidate, CandidateDTO> {
    public CandidateValidator() {
    }

    public void initialize(ValidCandidate validCandidate) {
    }

    public boolean isValid(CandidateDTO candidateDTO, ConstraintValidatorContext constraintValidatorContext) {
        boolean isValid = true;
        if (StringUtils.isEmpty(candidateDTO.getName())) {
            isValid = false;
        }
        return isValid;
    }
}
