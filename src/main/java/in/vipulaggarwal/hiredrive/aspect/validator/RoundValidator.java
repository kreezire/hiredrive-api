package in.vipulaggarwal.hiredrive.aspect.validator;

import in.vipulaggarwal.hiredrive.aspect.ValidRound;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import in.vipulaggarwal.hiredrive.enums.EntityType;
import in.vipulaggarwal.hiredrive.resources.CandidateResource;
import in.vipulaggarwal.hiredrive.services.CandidateService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import in.vipulaggarwal.hiredrive.dto.RoundDTO;

public class RoundValidator implements ConstraintValidator<ValidRound, RoundDTO> {
	private static final Logger logger = LoggerFactory.getLogger(CandidateResource.class);

	public RoundValidator() {
	}

	public void initialize(ValidRound ValidRound) {
	}

	@Autowired
	CandidateService candidateService;

	public boolean isValid(RoundDTO roundDTO, ConstraintValidatorContext constraintValidatorContext) {
		boolean isValid = true;
		if (StringUtils.isEmpty(roundDTO.getName())) {
			isValid = false;
		}

		if (roundDTO.getEntityType() == EntityType.CANDIDATE) {
			try {
				candidateService.getCandidateById(roundDTO.getEntityId());
			} catch (Exception e) {
				logger.error("Failed Candidate GET.");
				isValid = false;
			}
		} else if (roundDTO.getEntityType() == EntityType.DRIVE) {
			// TODO: for DRIVE
		} else {
			isValid = false;
		}

		return isValid;
	}
}