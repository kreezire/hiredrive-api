package in.vipulaggarwal.hiredrive.aspect.validator;

import in.vipulaggarwal.hiredrive.aspect.ValidDrive;
import in.vipulaggarwal.hiredrive.dto.DriveDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class DriveValidator implements ConstraintValidator<ValidDrive, DriveDTO> {
    public DriveValidator() {
    }

    public void initialize(ValidDrive ValidDrive) {
    }

    public boolean isValid(DriveDTO driveDTO, ConstraintValidatorContext constraintValidatorContext) {
        boolean isValid = true;
        if (StringUtils.isEmpty(driveDTO.getName())) {
        	isValid = false;
        }
        return isValid;
    }
}
