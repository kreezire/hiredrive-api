package in.vipulaggarwal.hiredrive.aspect.validator;

import in.vipulaggarwal.hiredrive.aspect.ValidInterview;
import in.vipulaggarwal.hiredrive.dto.CandidateDTO;
import in.vipulaggarwal.hiredrive.dto.InterviewDTO;
import in.vipulaggarwal.hiredrive.dto.RoundDTO;
import in.vipulaggarwal.hiredrive.enums.EntityType;
import in.vipulaggarwal.hiredrive.services.CandidateService;
import in.vipulaggarwal.hiredrive.services.RoundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class InterviewValidator implements ConstraintValidator<ValidInterview, InterviewDTO> {

    private static final Logger logger = LoggerFactory.getLogger(InterviewValidator.class);

    @Autowired
    CandidateService candidateService;

    @Autowired
    RoundService roundService;

    public InterviewValidator() {
    }

    public void initialize(ValidInterview validInterview) {
    }

    public boolean isValid(InterviewDTO interviewDTO, ConstraintValidatorContext constraintValidatorContext) {
        boolean isValid = true;

        isValid &= interviewDTO.getCandidateId() != null && interviewDTO.getRoundId() != null;

        CandidateDTO candidateDTO = null;
        RoundDTO roundDTO = null;
        if (isValid) {
            try {
                candidateDTO = candidateService.getCandidateById(interviewDTO.getCandidateId());
                roundDTO = roundService.getRoundByID(interviewDTO.getRoundId());

            } catch (Exception e) {
                logger.error("Invalid interviewDTO={}.", interviewDTO);
                isValid = false;
            }

            if (roundDTO.getEntityType() == EntityType.CANDIDATE && candidateDTO.getId() != roundDTO.getEntityId()) {
                isValid = false;
            } else if (roundDTO.getEntityType() == EntityType.DRIVE && candidateDTO.getDriveId() != roundDTO.getEntityId()) {
                isValid = false;
            }
        }


        return isValid;
    }
}
