package in.vipulaggarwal.hiredrive.aspect.validator;

import in.vipulaggarwal.hiredrive.aspect.ValidInterviewer;
import in.vipulaggarwal.hiredrive.dto.InterviewerDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class InterviewerValidator implements ConstraintValidator<ValidInterviewer, InterviewerDTO> {

	private static final Logger logger = LoggerFactory.getLogger(InterviewerValidator.class);

	public InterviewerValidator() {
	}

	public void initialize(ValidInterviewer validInterview) {

	}

	public boolean isValid(InterviewerDTO interviewerDTO, ConstraintValidatorContext constraintValidatorContext) {
		boolean isValid = true;

		isValid &= interviewerDTO != null && interviewerDTO.getEmailId() != null && interviewerDTO.getDriveId() != null;

		return isValid;
	}
}
