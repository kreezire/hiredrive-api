package in.vipulaggarwal.hiredrive.aspect;

import in.vipulaggarwal.hiredrive.aspect.validator.BasicUserAuthValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(
        validatedBy = {BasicUserAuthValidator.class}
)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidBasicUserAuth {
    String message() default "User Details are not valid.";

    Class<?>[] groups() default {};

    String[] values() default {};

    Class<? extends Payload>[] payload() default {};
}
