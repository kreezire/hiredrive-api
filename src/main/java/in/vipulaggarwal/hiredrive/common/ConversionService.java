package in.vipulaggarwal.hiredrive.common;

import in.vipulaggarwal.hiredrive.common.converters.BasicUserAuthDTOToBasicUserAuthQOConverter;
import in.vipulaggarwal.hiredrive.common.converters.BasicUserAuthDTOToBasicUserQOConverter;
import in.vipulaggarwal.hiredrive.common.converters.BasicUserAuthQOToUserDetailsConverter;
import in.vipulaggarwal.hiredrive.common.converters.BasicUserQOToBasicUserDTOConverter;
import in.vipulaggarwal.hiredrive.common.converters.CandidateDTOToCandidateQOConverter;
import in.vipulaggarwal.hiredrive.common.converters.CandidateQOToCandidateDTOConverter;
import in.vipulaggarwal.hiredrive.common.converters.DriveDTOToDriveQOConverter;
import in.vipulaggarwal.hiredrive.common.converters.DriveQOToDriveDTOConverter;
import in.vipulaggarwal.hiredrive.common.converters.InterviewCandidateRoundQOToInterviewCandidateRoundDTOConverter;
import in.vipulaggarwal.hiredrive.common.converters.InterviewDTOToInterviewQOConverter;
import in.vipulaggarwal.hiredrive.common.converters.InterviewQOToInterviewDTOConverter;
import in.vipulaggarwal.hiredrive.common.converters.InterviewerDTOToInterviewerQOConverter;
import in.vipulaggarwal.hiredrive.common.converters.InterviewerQOToInterviewerDTOConverter;
import in.vipulaggarwal.hiredrive.common.converters.RoundDTOToRoundQOConverter;
import in.vipulaggarwal.hiredrive.common.converters.RoundQOToRoundDTOConverter;
import io.jsonwebtoken.lang.Assert;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.support.DefaultConversionService;

import java.util.List;

public class ConversionService extends DefaultConversionService {
	public ConversionService() {
		this.addConverter(new BasicUserAuthDTOToBasicUserAuthQOConverter());
		this.addConverter(new BasicUserAuthDTOToBasicUserQOConverter());
		this.addConverter(new DriveDTOToDriveQOConverter());
		this.addConverter(new DriveQOToDriveDTOConverter());
		this.addConverter(new CandidateDTOToCandidateQOConverter());
		this.addConverter(new CandidateQOToCandidateDTOConverter());
		this.addConverter(new BasicUserAuthQOToUserDetailsConverter());
		this.addConverter(new BasicUserQOToBasicUserDTOConverter());
		this.addConverter(new RoundDTOToRoundQOConverter());
		this.addConverter(new RoundQOToRoundDTOConverter());
		this.addConverter(new InterviewDTOToInterviewQOConverter());
		this.addConverter(new InterviewQOToInterviewDTOConverter());
		this.addConverter(new InterviewerDTOToInterviewerQOConverter());
		this.addConverter(new InterviewerQOToInterviewerDTOConverter());
		this.addConverter(new InterviewCandidateRoundQOToInterviewCandidateRoundDTOConverter());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T> List<T> convert(List<?> sourceList, Class<T> targetClass) {
		Assert.notNull(sourceList, "Cannot convert null list.");
		return (List) this.convert(sourceList,
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(sourceList.get(0).getClass())),
				TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(targetClass)));

	}
}
