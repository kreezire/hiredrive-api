package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.BasicUserAuthQO;
import in.vipulaggarwal.hiredrive.db.qo.BasicUserQO;
import in.vipulaggarwal.hiredrive.dto.BasicUserAuthDTO;
import org.springframework.core.convert.converter.Converter;

public class BasicUserAuthDTOToBasicUserQOConverter implements Converter<BasicUserAuthDTO, BasicUserQO> {
	public BasicUserAuthDTOToBasicUserQOConverter() {

	}

	public BasicUserQO convert(BasicUserAuthDTO basicUserAuthDTO) {
		BasicUserQO basicUserQO = new BasicUserQO();
		basicUserQO.setFirstName(basicUserAuthDTO.getFirstName());
		basicUserQO.setLastName(basicUserAuthDTO.getLastName());
		basicUserQO.setEmailId(basicUserAuthDTO.getEmailId().toLowerCase());
		return basicUserQO;
	}
}
