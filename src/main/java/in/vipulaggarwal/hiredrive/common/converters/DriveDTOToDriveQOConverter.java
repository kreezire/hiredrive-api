package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.DriveQO;
import in.vipulaggarwal.hiredrive.dto.DriveDTO;

import org.springframework.core.convert.converter.Converter;

public class DriveDTOToDriveQOConverter implements Converter<DriveDTO, DriveQO> {
    public DriveDTOToDriveQOConverter() {

    }

    public DriveQO convert(DriveDTO driveDTO) {
    	DriveQO driveQO = new DriveQO();
    	driveQO.setId(driveDTO.getId());
    	driveQO.setName(driveDTO.getName());
    	driveQO.setStatus(driveDTO.getStatus().toString());
        return driveQO;
    }
}