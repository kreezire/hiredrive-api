package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.InterviewerQO;
import in.vipulaggarwal.hiredrive.dto.InterviewerDTO;
import org.springframework.core.convert.converter.Converter;

public class InterviewerQOToInterviewerDTOConverter implements Converter<InterviewerQO, InterviewerDTO> {

	public InterviewerDTO convert(InterviewerQO interviewerQO) {
		InterviewerDTO interviewerDTO = new InterviewerDTO();
		interviewerDTO.setId(interviewerQO.getId());
		interviewerDTO.setEmailId(interviewerQO.getEmailId());
		interviewerDTO.setDriveId(interviewerQO.getDriveId());
		return interviewerDTO;

	}
}
