package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.InterviewerQO;
import in.vipulaggarwal.hiredrive.dto.InterviewerDTO;
import org.springframework.core.convert.converter.Converter;

public class InterviewerDTOToInterviewerQOConverter implements Converter<InterviewerDTO, InterviewerQO> {

	public InterviewerQO convert(InterviewerDTO interviewerDTO) {
		InterviewerQO interviewerQO = new InterviewerQO();

		interviewerQO.setId(interviewerDTO.getId());
		interviewerQO.setEmailId(interviewerDTO.getEmailId());
		interviewerQO.setDriveId(interviewerDTO.getDriveId());
		return interviewerQO;
	}
}
