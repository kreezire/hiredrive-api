package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.CandidateQO;
import in.vipulaggarwal.hiredrive.dto.CandidateDTO;
import in.vipulaggarwal.hiredrive.enums.CandidateStatus;
import in.vipulaggarwal.hiredrive.enums.Gender;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;
import org.springframework.core.convert.converter.Converter;

public class CandidateQOToCandidateDTOConverter implements Converter<CandidateQO, CandidateDTO> {
    public CandidateQOToCandidateDTOConverter() {

    }

    public CandidateDTO convert(CandidateQO candidateQO) {
        CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setId(candidateQO.getId());
        String name = candidateQO.getName();
        if (StringUtils.isEmpty(name)) {
            name = "Anonymous";
        }

        candidateDTO.setName(WordUtils.capitalizeFully(name));
        candidateDTO.setPhone(candidateQO.getPhone());
        candidateDTO.setEmailId(candidateQO.getEmailId());
        candidateDTO.setDriveId(candidateQO.getDriveId());
        //TODO change external path to internal path - reverse
        candidateDTO.setResumePath(candidateQO.getResumePath());
        candidateDTO.setStatus(CandidateStatus.fromString(candidateQO.getStatus()));
        candidateDTO.setTalentPartner(candidateQO.getTalentPartner());
        candidateDTO.setYoe(candidateQO.getYoe());
        Gender candidateGender = candidateQO.getGender() == null ? Gender.UNSPECIFIED : Gender.valueOf(candidateQO.getGender());
        candidateDTO.setGender(candidateGender);
        candidateDTO.setSkills(candidateQO.getSkills());
        candidateDTO.setCreatedTime(candidateQO.getCreatedTime());
        return candidateDTO;
    }
}
