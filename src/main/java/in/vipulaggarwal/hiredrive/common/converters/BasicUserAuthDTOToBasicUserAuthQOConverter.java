package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.BasicUserAuthQO;
import in.vipulaggarwal.hiredrive.dto.BasicUserAuthDTO;
import org.springframework.core.convert.converter.Converter;

public class BasicUserAuthDTOToBasicUserAuthQOConverter implements Converter<BasicUserAuthDTO, BasicUserAuthQO> {
	public BasicUserAuthDTOToBasicUserAuthQOConverter() {

	}

	public BasicUserAuthQO convert(BasicUserAuthDTO basicUserAuthDTO) {
		BasicUserAuthQO basicUserAuthQO = new BasicUserAuthQO();
		basicUserAuthQO.setEmailId(basicUserAuthDTO.getEmailId().toLowerCase());
		// TODO password encryption/decryption
		basicUserAuthQO.setPassword(basicUserAuthDTO.getPassword());
		return basicUserAuthQO;
	}
}
