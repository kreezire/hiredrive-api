package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.InterviewQO;
import in.vipulaggarwal.hiredrive.dto.InterviewDTO;
import org.springframework.core.convert.converter.Converter;

public class InterviewDTOToInterviewQOConverter implements Converter<InterviewDTO, InterviewQO> {

	public InterviewQO convert(InterviewDTO interviewDTO) {
		InterviewQO interviewQO = new InterviewQO();

		interviewQO.setId(interviewDTO.getId());
		interviewQO.setStartTime(interviewDTO.getStartTime());
		interviewQO.setRating(interviewDTO.getRating());
		interviewQO.setSubmitTime(interviewDTO.getSubmitTime());
		interviewQO.setInterviewer(interviewDTO.getInterviewer().toLowerCase());
		interviewQO.setFeedback(interviewDTO.getFeedback());
		if (interviewDTO.getStatus() != null)
			interviewQO.setStatus(interviewDTO.getStatus().toString());
		interviewQO.setCandidateId(interviewDTO.getCandidateId());
		interviewQO.setRoundId(interviewDTO.getRoundId());
		interviewQO.setFeedbackDraft(interviewDTO.getFeedbackDraft());
		interviewQO.setDuration(interviewDTO.getDuration());
		return interviewQO;
	}
}
