package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.RoundQO;
import in.vipulaggarwal.hiredrive.dto.RoundDTO;
import org.springframework.core.convert.converter.Converter;

public class RoundDTOToRoundQOConverter implements Converter<RoundDTO, RoundQO> {
	public RoundDTOToRoundQOConverter() {

	}

	public RoundQO convert(RoundDTO roundDTO) {
		RoundQO roundQO = new RoundQO();
		roundQO.setId(roundDTO.getId());
		roundQO.setName(roundDTO.getName());
		roundQO.setEntityId(roundDTO.getEntityId());
		if (roundDTO.getEntityType() != null)
			roundQO.setEntityType(roundDTO.getEntityType().toString());
		roundQO.setRequired(roundDTO.isRequired());

		return roundQO;
	}
}
