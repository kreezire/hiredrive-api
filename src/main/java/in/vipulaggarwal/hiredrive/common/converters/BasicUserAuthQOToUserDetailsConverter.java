package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.BasicUserAuthQO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.User.UserBuilder;

import java.util.ArrayList;

public class BasicUserAuthQOToUserDetailsConverter implements Converter<BasicUserAuthQO, UserDetails> {
    public BasicUserAuthQOToUserDetailsConverter() {

    }

    public UserDetails convert(BasicUserAuthQO basicUserAuthQO) {
        UserBuilder userBuilder = User.withUsername(basicUserAuthQO.getEmailId())
                .password(basicUserAuthQO.getPassword())
                .authorities(new ArrayList<>());
        return userBuilder.build();
    }
}
