package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.RoundQO;
import in.vipulaggarwal.hiredrive.dto.RoundDTO;
import in.vipulaggarwal.hiredrive.enums.EntityType;
import org.springframework.core.convert.converter.Converter;

public class RoundQOToRoundDTOConverter implements Converter<RoundQO, RoundDTO> {
	public RoundQOToRoundDTOConverter() {

	}

	public RoundDTO convert(RoundQO roundQO) {
		RoundDTO roundDTO = new RoundDTO();
		roundDTO.setId(roundQO.getId());
		roundDTO.setName(roundQO.getName());
		roundDTO.setEntityId(roundQO.getEntityId());
		roundDTO.setEntityType(EntityType.fromString(roundQO.getEntityType()));
		roundDTO.setRequired(roundQO.isRequired());
		return roundDTO;
	}
}
