package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.BasicUserQO;
import in.vipulaggarwal.hiredrive.dto.BasicUserDTO;
import in.vipulaggarwal.hiredrive.dto.impl.BasicUserDTOImpl;
import org.springframework.core.convert.converter.Converter;

public class BasicUserQOToBasicUserDTOConverter implements Converter<BasicUserQO, BasicUserDTO> {
    public BasicUserQOToBasicUserDTOConverter() {

    }

    public BasicUserDTO convert(BasicUserQO basicUserQO) {
        BasicUserDTO basicUserDTO = new BasicUserDTOImpl();

        basicUserDTO.setFirstName(basicUserQO.getFirstName());
        basicUserDTO.setLastName(basicUserQO.getLastName());
        basicUserDTO.setEmailId(basicUserQO.getEmailId());
        return basicUserDTO;
    }
}
