package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.InterviewQO;
import in.vipulaggarwal.hiredrive.dto.InterviewDTO;
import in.vipulaggarwal.hiredrive.enums.InterviewStatus;
import org.springframework.core.convert.converter.Converter;

public class InterviewQOToInterviewDTOConverter implements Converter<InterviewQO, InterviewDTO> {

	public InterviewDTO convert(InterviewQO interviewQO) {
		InterviewDTO interviewDTO = new InterviewDTO();
		interviewDTO.setId(interviewQO.getId());
		interviewDTO.setStartTime(interviewQO.getStartTime());
		interviewDTO.setRating(interviewQO.getRating());
		interviewDTO.setSubmitTime(interviewQO.getSubmitTime());
		interviewDTO.setInterviewer(interviewQO.getInterviewer());
		interviewDTO.setFeedback(interviewQO.getFeedback());
		interviewDTO.setStatus(InterviewStatus.fromString(interviewQO.getStatus()));
		interviewDTO.setCandidateId(interviewQO.getCandidateId());
		interviewDTO.setRoundId(interviewQO.getRoundId());
		interviewDTO.setFeedbackDraft(interviewQO.getFeedbackDraft());
		interviewDTO.setDuration(interviewQO.getDuration());
		return interviewDTO;

	}
}
