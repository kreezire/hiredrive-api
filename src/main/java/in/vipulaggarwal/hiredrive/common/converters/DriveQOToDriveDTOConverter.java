package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.DriveQO;
import in.vipulaggarwal.hiredrive.dto.DriveDTO;

import in.vipulaggarwal.hiredrive.enums.DriveStatus;

import org.springframework.core.convert.converter.Converter;

public class DriveQOToDriveDTOConverter implements Converter<DriveQO, DriveDTO> {
    public DriveQOToDriveDTOConverter() {

    }

    public DriveDTO convert(DriveQO driveQO) {
    	DriveDTO driveDTO = new DriveDTO();
    	driveDTO.setId(driveQO.getId());
    	driveDTO.setName(driveQO.getName());
    	driveDTO.setStatus(DriveStatus.fromString(driveQO.getStatus()));
    	return driveDTO;
    }
}
