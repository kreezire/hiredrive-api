package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.CandidateQO;
import in.vipulaggarwal.hiredrive.dto.CandidateDTO;
import org.springframework.core.convert.converter.Converter;

public class CandidateDTOToCandidateQOConverter implements Converter<CandidateDTO, CandidateQO> {
    public CandidateDTOToCandidateQOConverter() {}

    public CandidateQO convert(CandidateDTO candidateDTO) {
        CandidateQO candidateQO = new CandidateQO();
        candidateQO.setId(candidateDTO.getId());
        candidateQO.setName(candidateDTO.getName());
        candidateQO.setPhone(candidateDTO.getPhone());
        candidateQO.setEmailId(candidateDTO.getEmailId().toLowerCase());
        candidateQO.setDriveId(candidateDTO.getDriveId());
        //TODO change external path to internal path
        candidateQO.setResumePath(candidateDTO.getResumePath());
        candidateQO.setStatus(candidateDTO.getStatus().toString());
        candidateQO.setTalentPartner(candidateDTO.getTalentPartner());
        candidateQO.setYoe(candidateDTO.getYoe());
        candidateQO.setGender(candidateDTO.getGender().toString());
        candidateQO.setSkills(candidateDTO.getSkills());
        return candidateQO;
    }
}
