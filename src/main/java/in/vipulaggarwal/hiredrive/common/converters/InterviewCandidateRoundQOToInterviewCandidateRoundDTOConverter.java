package in.vipulaggarwal.hiredrive.common.converters;

import in.vipulaggarwal.hiredrive.db.qo.InterviewCandidateRoundQO;
import in.vipulaggarwal.hiredrive.dto.InterviewCandidateRoundDTO;
import in.vipulaggarwal.hiredrive.enums.InterviewStatus;

import org.springframework.core.convert.converter.Converter;

public class InterviewCandidateRoundQOToInterviewCandidateRoundDTOConverter
		implements Converter<InterviewCandidateRoundQO, InterviewCandidateRoundDTO> {

	public InterviewCandidateRoundDTO convert(InterviewCandidateRoundQO interviewCandidateRoundQO) {
		InterviewCandidateRoundDTO interviewCandidateRoundDTO = new InterviewCandidateRoundDTO();
		interviewCandidateRoundDTO.setInterviewId(interviewCandidateRoundQO.getInterviewId());
		interviewCandidateRoundDTO
				.setInterviewStatus(InterviewStatus.fromString(interviewCandidateRoundQO.getInterviewStatus()));
		interviewCandidateRoundDTO.setRoundName(interviewCandidateRoundQO.getRoundName());
		interviewCandidateRoundDTO.setCandidateId(interviewCandidateRoundQO.getCandidateId());
		interviewCandidateRoundDTO.setCandidateName(interviewCandidateRoundQO.getCandidateName());
		return interviewCandidateRoundDTO;
	}
}
