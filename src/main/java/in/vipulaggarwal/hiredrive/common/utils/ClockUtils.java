package in.vipulaggarwal.hiredrive.common.utils;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class ClockUtils {
    private ClockUtils() {

    }

    public static Timestamp getCurrentTimestamp() {
        LocalDateTime ldt = LocalDateTime.ofInstant(Instant.now(), ZoneOffset.UTC);
        Timestamp timestamp = Timestamp.valueOf(ldt);
        return timestamp;
    }
    
    public static Long getUnixMiliSeconds() {
    	 return Instant.now().getEpochSecond();
    }
}
