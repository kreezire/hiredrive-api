package in.vipulaggarwal.hiredrive.common.utils;

import com.google.common.collect.ImmutableList;
import in.vipulaggarwal.hiredrive.enums.CandidateStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CandidateHelper {
	private CandidateHelper() {

	}

	private static Map<CandidateStatus, List<CandidateStatus>> candidateStatusTransitionMap;

	static {
		candidateStatusTransitionMap = new HashMap<>();

		candidateStatusTransitionMap.put(CandidateStatus.NEW,
				ImmutableList.of(CandidateStatus.REJECTED, CandidateStatus.IN_PROGRESS));
		candidateStatusTransitionMap.put(CandidateStatus.IN_PROGRESS, ImmutableList.of(CandidateStatus.WAITING));
		candidateStatusTransitionMap.put(CandidateStatus.WAITING,
				ImmutableList.of(CandidateStatus.IN_PROGRESS, CandidateStatus.REJECTED, CandidateStatus.SELECTED));
		candidateStatusTransitionMap.put(CandidateStatus.SELECTED, ImmutableList.of(CandidateStatus.REJECTED));
	}

	public static boolean isValidStatusTransition(CandidateStatus oldStatus, CandidateStatus newStatus) {
		boolean isValid = false;
		if ((oldStatus == newStatus) || (candidateStatusTransitionMap.containsKey(oldStatus)
				&& candidateStatusTransitionMap.get(oldStatus).contains(newStatus))) {
			isValid = true;
		}
		return isValid;
	}
}
