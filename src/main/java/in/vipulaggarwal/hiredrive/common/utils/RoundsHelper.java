package in.vipulaggarwal.hiredrive.common.utils;

import in.vipulaggarwal.hiredrive.db.qo.RoundQO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class RoundsHelper {
	private static final Logger logger = LoggerFactory.getLogger(RoundsHelper.class);

	private RoundsHelper() {

	}

	public static void addCreateUserId(List<RoundQO> roundQOList, String userId) {
		if (roundQOList != null) {
			for (RoundQO roundQO : roundQOList) {
				roundQO.setCreatedUserId(userId);
			}
		}
	}

	public static void addCreateUserId(RoundQO roundQO, String userId) {
		if (roundQO != null) {
			roundQO.setCreatedUserId(userId);
		}
	}

}
