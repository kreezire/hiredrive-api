package in.vipulaggarwal.hiredrive.common.utils;

import in.vipulaggarwal.hiredrive.dto.InterviewDTO;
import in.vipulaggarwal.hiredrive.enums.InterviewStatus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InterviewHelper {

    private static final Logger logger = LoggerFactory.getLogger(InterviewHelper.class);

    private InterviewHelper() {

    }

    private static boolean isValidInterviewerUpdate(String newInterviewer, String oldInterviewer, InterviewStatus status) {
        boolean isValid = StringUtils.equals(newInterviewer, oldInterviewer) || status.equals(InterviewStatus.PENDING);
        logger.info("isValidInterviewerUpdate={}", isValid);
        return isValid;
    }

    public static boolean isValidForUpdate(InterviewDTO oldInterviewDTO, InterviewDTO newInterviewDTO) {
        boolean isValid = true;

        isValid &= isValidInterviewerUpdate(newInterviewDTO.getInterviewer(), oldInterviewDTO.getInterviewer(), newInterviewDTO.getStatus());

        return isValid;
    }

    public static boolean isValidInterviewerStartUpdate(String newInterviewer, InterviewStatus newStatus,
                                                        InterviewStatus oldStatus) {
        boolean isValid = newStatus.equals(InterviewStatus.ONGOING) && oldStatus.equals(InterviewStatus.PENDING)
                && !StringUtils.isEmpty(newInterviewer);
        logger.info("isValidInterviewerStartUpdate={}", isValid);
        return isValid;
    }

    public static boolean isValidInterviewerSubmitUpdate(String interviewer, String oldInterviewer, InterviewStatus newStatus,
                                                         InterviewStatus oldStatus) {
        boolean isValid = interviewer.equals(oldInterviewer) && newStatus.equals(InterviewStatus.DONE) && oldStatus.equals(InterviewStatus.ONGOING)
                && !StringUtils.isEmpty(interviewer);
        logger.info("isValidInterviewerStartUpdate={}", isValid);
        return isValid;
    }

    public static boolean isValidRating(Integer rating) {
        return rating != null && rating >= 1 && rating <= 5;
    }

    public static InterviewDTO getDummyInterview(Integer roundId, Integer candidateId) {
        InterviewDTO interviewDTO = new InterviewDTO();
        interviewDTO.setStatus(InterviewStatus.PENDING);
        interviewDTO.setRoundId(roundId);
        interviewDTO.setCandidateId(candidateId);
        return interviewDTO;

    }
}
