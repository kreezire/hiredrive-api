package in.vipulaggarwal.hiredrive.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.vipulaggarwal.hiredrive.db.qo.CandidateQO;
import in.vipulaggarwal.hiredrive.exceptions.HDException;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public class ObjUtils {

	private static final Logger logger = LoggerFactory.getLogger(ObjUtils.class);
	private static final ObjectMapper mapper = new ObjectMapper();

	public ObjUtils() {

	}

	public static Map<String, Object> getObjectFromJson(String json) {
		Map<String, Object> resultObject = null;
		if (json != null) {
			try {
				resultObject = (Map) mapper.readValue(json, new TypeReference<Map<String, Object>>() {
				});
			} catch (IOException var3) {
				logger.error("Unable to parse json from string={}", json);
				throw new HDException("Unable to parse json from string");
			}
		}

		return resultObject;
	}

	public static Object getObjectFromJson(String json, Class clazz) {
		try {
			return mapper.readValue(json, clazz);
		} catch (IOException var3) {
			logger.error("Unable to parse json from string={}", json, var3);
			throw new HDException("Unable to parse json from string");
		}
	}

	public static Object getObjectFromJson(String json, TypeReference typeReference) {
		try {
			return mapper.readValue(json, typeReference);
		} catch (IOException var3) {
			logger.error("Unable to parse object from json string={}", json);
			throw new HDException("Unable to parse object from json string");
		}
	}

	public static String getJsonFromObject(Map<String, Object> map) {
		try {
			return mapper.writeValueAsString(map);
		} catch (JsonProcessingException var2) {
			logger.error("Unable to convert json from map={} ", map);
			throw new HDException("Unable to convert json from map");
		}
	}

	public static String getJsonFromObject(Object object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException var2) {
			logger.error("Unable to convert json from object={} ", object, var2);
			throw new HDException("Unable to convert json from Object");
		}
	}

	public static boolean validFile(FormDataContentDisposition fileDetails) {
		if (fileDetails == null || fileDetails.getFileName() == null || fileDetails.getFileName().length() == 0)
			return false;
		return true;
	}

	public static boolean validForAddCandidateQO(CandidateQO candidateQO) {
		boolean isValid = true;

		if (candidateQO == null) {
			isValid = false;
		} else if (candidateQO.getName() == null && candidateQO.getEmailId() == null
				&& candidateQO.getPhone() == null && candidateQO.getDriveId() == null) {
			isValid = false;
		}

		return isValid;
	}

}
