package in.vipulaggarwal.hiredrive.common.utils;

public class ResumeHelper {
    private ResumeHelper() {

    }

    public static String getResumePath(Integer candidateId, String fileName) {
        return candidateId + "/" + fileName;
    }
}
