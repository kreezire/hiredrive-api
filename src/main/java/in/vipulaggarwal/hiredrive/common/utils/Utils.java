package in.vipulaggarwal.hiredrive.common.utils;

import in.vipulaggarwal.hiredrive.exceptions.HDException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.Key;
import java.sql.Date;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class Utils {
	private static final Logger logger = LoggerFactory.getLogger(Utils.class);

	private Utils() {

	}

	public static String getURL(String url, Map<String, String> uriVariables) {
		URIBuilder uriBuilder;
		URI uri;
		try {
			uriBuilder = new URIBuilder(url);
			if (!CollectionUtils.isEmpty(uriVariables)) {
				for (String key : uriVariables.keySet()) {
					uriBuilder.addParameter(key, uriVariables.get(key));
				}
			}
			uri = uriBuilder.build();
			return uri.toURL().toString();
		} catch (URISyntaxException e) {
			logger.error("URI exception ,unable to build URI={} due to syntax error", url);
			throw new HDException("URI exception ,unable to build URI", e);
		} catch (MalformedURLException e) {
			logger.error("URL exception ,unable to create URL={} due malformed url", url);
			throw new HDException("URL exception ,unable to create URL", e);
		}
	}

	public static boolean doesURLWorking(String urlString) throws IOException {
		URL url = new URL(urlString);
		// We want to check the current URL
		HttpURLConnection.setFollowRedirects(false);

		HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

		// We don't need to get data
		httpURLConnection.setRequestMethod("HEAD");

		// Some websites don't like programmatic access so pretend to be a browser
		httpURLConnection.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
		int responseCode = httpURLConnection.getResponseCode();

		// We only accept response code 200
		return responseCode == HttpURLConnection.HTTP_OK;
	}

	public static String createJWT(String id, String issuer, String subject, long ttlMillis) {

		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		// We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("hiringnote");
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		// Let's set the JWT Claims
		JwtBuilder builder = Jwts.builder().setId(id).setIssuedAt(now).setSubject(subject).setIssuer(issuer)
				.signWith(signatureAlgorithm, signingKey);

		// if it has been specified, let's add the expiration
		if (ttlMillis > 0) {
			long expMillis = nowMillis + ttlMillis;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}

		// Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}

	public static Claims decodeJWT(String jwt) throws io.jsonwebtoken.SignatureException {
		// This line will throw an exception if it is not a signed JWT
		Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary("hiringnote"))
				.parseClaimsJws(jwt).getBody();
		return claims;
	}

}
